import React, { useState } from 'react'

import './Settings.css'
import { useHistory } from 'react-router-dom'
import { AdminPanelNavigation, NavBar, TextInput } from '../../../components'
import background from '../../../assets/raster/admin-background.png'

const Settings = () => {
  const history = useHistory()

  const [email, setEmail] = useState('')
  const [currentPassword, setCurrentPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')

  const [emailValidity, setEmailValidity] = useState(null)
  const [currentPasswordValidity, setCurrentPasswordValidity] = useState(null)
  const [newPasswordValidity, setNewPasswordValidity] = useState(null)
  const [repeatPasswordValidity, setRepeatPasswordValidity] = useState(null)

  const checkValidity = () => {
    let valid = true

    if (email === '') {
      setEmailValidity('Invalid email')
      valid = false
    } else {
      setEmailValidity(null)
    }

    if (currentPassword === '') {
      setCurrentPasswordValidity('Incorrect current password')
      valid = false
    } else {
      setCurrentPasswordValidity(null)
    }

    if (newPassword === '') {
      setNewPasswordValidity('Invalid new password')
      valid = false
    } else {
      setNewPasswordValidity(null)
    }

    if (repeatPassword !== newPassword) {
      setRepeatPasswordValidity('Password mismatch')
      valid = false
    } else {
      setRepeatPasswordValidity(null)
    }

    return valid
  }

  return (
    <div
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      }}
    >
      <NavBar isSignedIn />

      <form className="personal-settings">
        <p className="admin-panel-title">Settings</p>
        <p className="lime-text" style={{ margin: '40px 0' }}>
          Set up a new password.
        </p>
        <div className="personal-settings-inputs">
          <div>
            <TextInput
              styleId={4}
              title="e-mail address"
              value={email}
              additionalStyle={{ margin: 0 }}
              onChange={(e) => setEmail(e.target.value)}
            />
            {emailValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {emailValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              styleId={4}
              title="CURRENT PASSWORD *"
              value={currentPassword}
              isPassword
              additionalStyle={{ margin: 0 }}
              onChange={(e) => setCurrentPassword(e.target.valueOf())}
            />
            {currentPasswordValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {currentPasswordValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              styleId={4}
              title="NEW PASSWORD"
              value={newPassword}
              isPassword
              additionalStyle={{ margin: 0 }}
              onChange={(e) => setNewPassword(e.target.value)}
            />
            {newPasswordValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {newPasswordValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              styleId={4}
              title="repeat new password"
              value={repeatPassword}
              isPassword
              additionalStyle={{ margin: 0 }}
              onChange={(e) => setRepeatPassword(e.target.value)}
            />
            {repeatPasswordValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {repeatPasswordValidity}
              </p>
            ) : null}
          </div>
        </div>
      </form>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'change password',
            onClick: (e) => {
              if (!checkValidity()) e.preventDefault()
            },
            type: 'submit',
          },
        ]}
        buttonsLeft={[
          {
            label: 'Back',
            onClick: () => {
              history.push('/dashboard')
            },
          },
        ]}
      />
    </div>
  )
}

export default Settings
