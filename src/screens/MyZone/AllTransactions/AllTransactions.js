import React, { useEffect, useState } from 'react'

import '../MyZone.css'
import { useHistory } from 'react-router-dom'
import background from '../../../assets/raster/admin-background.png'
import leftArrow from '../../../assets/vector/left-lime-arrow.svg'
import { DropDown, NavBar, PrimaryButton, TextInput } from '../../../components'
import Pagination from '../../../components/Pagination/Pagination'

const transactions = [
  {
    date: '01/03/2021',
    get: '16 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/04/2021',
    get: '5 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Done',
  },
  {
    date: '01/05/2021',
    get: '3 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/06/2021',
    get: '9 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/07/2021',
    get: '8 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/08/2021',
    get: '1 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/03/2021',
    get: '16 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/04/2021',
    get: '5 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Done',
  },
  {
    date: '01/05/2021',
    get: '3 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/06/2021',
    get: '9 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/07/2021',
    get: '8 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/08/2021',
    get: '1 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/03/2021',
    get: '16 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/04/2021',
    get: '5 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Done',
  },
  {
    date: '01/05/2021',
    get: '3 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/06/2021',
    get: '9 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/07/2021',
    get: '8 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/08/2021',
    get: '1 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/03/2021',
    get: '16 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/04/2021',
    get: '5 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Done',
  },
  {
    date: '01/05/2021',
    get: '3 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/06/2021',
    get: '9 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
  {
    date: '01/07/2021',
    get: '8 tokens',
    pay: '452.144 ETH',
    equalsUSD: '233.000 USD',
    walletAddress: '0x07655a8d9c2c35dafe140ad995f27d399f177549',
    status: 'Pending',
  },
]

const AllTransactions = () => {
  const [isPendingChecked, setIsPendingChecked] = useState(true)
  const [isDoneChecked, setIsDoneChecked] = useState(true)
  // eslint-disable-next-line no-unused-vars
  const [transactionsToShow, setTransactionsToShow] = useState(transactions)
  const [walletAddressSearch, setWalletAddressSearch] = useState('')
  const [startDate, setStartDate] = useState(
    new Date(
      Math.min(
        ...transactions.map((transaction) => new Date(transaction.date)),
      ),
    ),
  )
  const [endDate, setEndDate] = useState(
    new Date(
      Math.max(
        ...transactions.map((transaction) => new Date(transaction.date)),
      ),
    ),
  )
  // eslint-disable-next-line no-unused-vars
  const [amountFrom, setAmountFrom] = useState(
    Math.min(
      ...transactions.map((transaction) =>
        parseInt(transaction.get.split(' ')[0], 10),
      ),
    ),
  )
  // eslint-disable-next-line no-unused-vars
  const [amountTo, setAmountTo] = useState(
    Math.max(
      ...transactions.map((transaction) =>
        parseInt(transaction.get.split(' ')[0], 10),
      ),
    ),
  )

  const history = useHistory()

  const [currentPage, setCurrentPage] = useState(1)
  const [transactionsPerPage] = useState(6)

  const indexOfLastTransaction = currentPage * transactionsPerPage
  const indexOfFirstTransaction = indexOfLastTransaction - transactionsPerPage
  const currentTransactions = transactionsToShow.slice(
    indexOfFirstTransaction,
    indexOfLastTransaction,
  )
  // const transactions = []

  useEffect(() => {
    let transactionsFiltered = transactions
      .filter((transaction) => {
        if (isPendingChecked && isDoneChecked)
          return (
            transaction.status === 'Pending' || transaction.status === 'Done'
          )
        if (isPendingChecked) return transaction.status === 'Pending'
        if (isDoneChecked) return transaction.status === 'Done'
        return null
      })
      .filter((transaction) =>
        transaction.walletAddress.includes(walletAddressSearch),
      )
      .filter(
        (transaction) =>
          parseInt(transaction.get.split(' ')[0], 10) <= amountTo &&
          parseInt(transaction.get.split(' ')[0], 10) >= amountFrom,
      )

    if (endDate !== null) {
      transactionsFiltered = transactionsFiltered.filter(
        (transaction) =>
          new Date(transaction.date) <= endDate &&
          new Date(transaction.date) >= startDate,
      )
    }

    setTransactionsToShow(transactionsFiltered)
  }, [
    isPendingChecked,
    isDoneChecked,
    walletAddressSearch,
    endDate,
    amountFrom,
    amountTo,
  ])

  return (
    <div
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      }}
    >
      <NavBar isSignedIn isExtended />

      {transactions.length > 0 ? (
        <>
          <div className="all-transactions-panel">
            <p className="admin-panel-title">ALL TRANSACTIONS</p>

            <div style={{ display: 'flex', margin: '40px 0 10px 0' }}>
              <DropDown
                label="Status"
                options={[
                  {
                    name: 'Pending',
                    handleOnCheck: () => setIsPendingChecked(!isPendingChecked),
                    checked: isPendingChecked,
                  },
                  {
                    name: 'Done',
                    handleOnCheck: () => setIsDoneChecked(!isDoneChecked),
                    checked: isDoneChecked,
                  },
                ]}
                type="multiple-choice"
              />
              <DropDown
                label="Date"
                type="date-range"
                additionalStyle={{ marginLeft: 20 }}
                startDate={startDate}
                endDate={endDate}
                setEndDate={setEndDate}
                setStartDate={setStartDate}
              />

              <DropDown
                label="Amount"
                type="number-range"
                additionalStyle={{ marginLeft: 20 }}
                onAmountFromChange={(e) =>
                  setAmountFrom(parseInt(e.target.value, 10))
                }
                onAmountToChange={(e) =>
                  setAmountTo(parseInt(e.target.value, 10))
                }
                amountFrom={amountFrom}
                amountTo={amountTo}
              />

              <TextInput
                placeholder="Search for DashboardExtended Address"
                styleId={3}
                onChange={(e) => setWalletAddressSearch(e.target.value)}
                additionalStyle={{ marginLeft: 20 }}
              />
            </div>

            <div className="transactions-table">
              <p className="column-title">DATE</p>
              <p className="column-title">GET</p>
              <p className="column-title">PAY</p>
              <p className="column-title">EQUALS USD</p>
              <p className="column-title">WALLET ADDRESS</p>
              <p className="column-title">STATUS</p>
              <p />
              <p />
              {currentTransactions.map((transaction, index) => (
                <>
                  <p
                    className="column-value"
                    style={
                      index === transactions.length - 1
                        ? { borderBottom: 'none' }
                        : {}
                    }
                  >
                    {transaction.date}
                  </p>
                  <p
                    className="column-value"
                    style={
                      index === transactions.length - 1
                        ? { borderBottom: 'none' }
                        : {}
                    }
                  >
                    {transaction.get}
                  </p>
                  <p
                    className="column-value"
                    style={
                      index === transactions.length - 1
                        ? { borderBottom: 'none' }
                        : {}
                    }
                  >
                    {transaction.pay}
                  </p>
                  <p
                    className="column-value"
                    style={
                      index === transactions.length - 1
                        ? { borderBottom: 'none' }
                        : {}
                    }
                  >
                    {transaction.equalsUSD}
                  </p>
                  <p
                    className="column-value"
                    style={{
                      paddingRight: 120,
                      ...(index === transactions.length - 1
                        ? { borderBottom: 'none' }
                        : {}),
                    }}
                  >
                    {transaction.walletAddress}
                  </p>
                  <p
                    className="column-value"
                    style={{
                      ...(transaction.status === 'Pending'
                        ? { color: '#FFA552' }
                        : { color: '#54F7C5' }),
                      ...(index === transactions.length - 1
                        ? { borderBottom: 'none' }
                        : {}),
                    }}
                  >
                    {transaction.status}
                  </p>
                  {transaction.status === 'Pending' ? (
                    <a
                      className="column-value"
                      style={{
                        color: '#D10286',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        justifyContent: 'center',
                        ...(index === transactions.length - 1
                          ? { borderBottom: 'none' }
                          : {}),
                      }}
                      href="/#"
                    >
                      CANCEL
                    </a>
                  ) : (
                    <p className="column-value" />
                  )}
                  {transaction.status === 'Pending' ? (
                    <a
                      className="column-value"
                      style={{
                        color: '#54F7C5',
                        textDecoration: 'underline',
                        justifyContent: 'flex-end',
                        ...(index === transactions.length - 1
                          ? { borderBottom: 'none' }
                          : {}),
                      }}
                      href="/payment/pay-with-provider"
                    >
                      PAY
                    </a>
                  ) : (
                    <a
                      className="column-value"
                      style={{
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        justifyContent: 'flex-end',
                      }}
                      href="/#"
                    >
                      RECEIPT
                    </a>
                  )}
                </>
              ))}
            </div>

            <Pagination
              dataPerPage={transactionsPerPage}
              totalData={transactionsToShow}
              paginate={(pageNumber) => setCurrentPage(pageNumber)}
            />
          </div>

          <PrimaryButton
            textContent="BACK TO DASHBOARD"
            icon={leftArrow}
            iconPositionIsLeft
            additionalStyle={{ marginLeft: 80 }}
            onClick={() => history.push('/dashboard')}
          />
        </>
      ) : (
        <div className="no-transactions-container">
          <p
            className="admin-panel-title"
            style={{ textAlign: 'center', width: '100%', marginBottom: 0 }}
          >
            you don’t have any transactions yet
          </p>
          <p
            className="white-text"
            style={{
              textAlign: 'center',
              width: '100%',
              margin: '40px 0 80px 0',
            }}
          >
            Buy tokens with Bitcoin, Ethereum, USDC or USDT.
          </p>
          <PrimaryButton
            textContent="BUY OUR MOGEN TOKEN"
            additionalStyle={{ alignSelf: 'center' }}
          />
        </div>
      )}
    </div>
  )
}

export default AllTransactions
