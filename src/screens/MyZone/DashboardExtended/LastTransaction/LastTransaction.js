import React from 'react'

import './LastTransaction.css'

import arrowRight from '../../../../assets/vector/right-lime-arrow.svg'
import { PrimaryButton } from '../../../../components'

const LastTransaction = () => (
  <div className="last-transaction-panel">
    <p className="admin-panel-title">LAST TRANSACTION</p>
    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      <div>
        <p className="column-title">Date</p>
        <p className="column-value">01/03/2021</p>
      </div>
      <div>
        <p className="column-title">TYPE (?)</p>
        <p className="column-value">Bought 12 tokens</p>
      </div>
      <div>
        <p className="column-title">AMOUNT</p>
        <p className="column-value">452.144 ETH</p>
      </div>
      <div>
        <p className="column-title">STATUS</p>
        <p className="column-value" style={{ color: '#FFA552' }}>
          Pending
        </p>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          flexDirection: 'column',
        }}
      >
        <a
          className="pay-link"
          href="/#"
          style={{ textDecoration: 'underline', margin: 0 }}
        >
          PAY
        </a>
      </div>
    </div>

    <PrimaryButton
      icon={arrowRight}
      textContent="SEE ALL TRANSACTIONS"
      additionalStyle={{ marginTop: 40 }}
    />
  </div>
)

export default LastTransaction
