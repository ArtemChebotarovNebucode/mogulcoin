import React from 'react'

import '../MyZone.css'
import background from '../../../assets/raster/admin-background.png'
import arrowRight from '../../../assets/vector/right-pink-arrow.svg'
import {
  BuyTokenPanel,
  CountDown,
  NavBar,
  TextInput,
} from '../../../components'

import TotalBalance from './TotalBalance'
import LastTransaction from './LastTransaction'

const DashboardExtended = () => (
  <div
    className="admin-screen"
    style={{ backgroundImage: `url(${background})` }}
  >
    <NavBar isSignedIn isExtended />
    <div className="admin-panel">
      <div className="admin-panel-left">
        <BuyTokenPanel isExtended />
      </div>
      <div className="admin-panel-right">
        <div className="profile-status">
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <p id="token-sales-live">Token sales is LIVE</p>
            <p className="white-text" style={{ marginLeft: 32 }}>
              Current bonus <span style={{ color: '#54F7C5' }}>18%</span>
            </p>
          </div>

          <CountDown dateToCountDown={new Date(2021, 4, 31)} />
        </div>

        <TotalBalance />

        <LastTransaction />

        <div className="profit-counter">
          <p className="admin-panel-title" style={{ marginBottom: 40 }}>
            HOW MUCH YOU WILL PROFIT?
          </p>

          <div style={{ display: 'flex', alignItems: 'center' }}>
            <TextInput
              title="IF YOU INVEST TODAY"
              insertion="USD"
              value="50.00"
              styleId={2}
              additionalStyle={{ width: '100%' }}
              type="number"
            />

            <img src={arrowRight} alt="Arrow" style={{ margin: '0 54px' }} />
            <TextInput
              title="YOUR PROFIT AT 5TH STAGE"
              insertion="USD"
              value="50.00"
              styleId={2}
              additionalStyle={{ width: '100%' }}
              type="number"
            />
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default DashboardExtended
