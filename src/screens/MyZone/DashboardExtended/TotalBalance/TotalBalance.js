import React from 'react'

import './TotalBalance.css'

import reloadIcon from '../../../../assets/vector/reload-icon.svg'
import optionsIcon from '../../../../assets/vector/options-icon.svg'
import ethLogo from '../../../../assets/vector/eth-logo.svg'
import btcLogo from '../../../../assets/vector/btc-logo.svg'
import usdtLogo from '../../../../assets/vector/usdt-logo.svg'
import usdcLogo from '../../../../assets/vector/usdc-logo.svg'

const TotalBalance = () => (
  <div className="total-balance-panel">
    <p className="lime-text" style={{ margin: 0 }}>
      Total balance
    </p>
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <p className="total-balance">300 112.02 USD</p>
        <button type="button" className="icon-button" style={{ marginTop: 20 }}>
          <img src={reloadIcon} alt="Reload" />
        </button>
      </div>

      <button type="button" className="icon-button">
        <img src={optionsIcon} alt="Options" />
      </button>
    </div>
    <p className="pink-text" style={{ margin: '40px 0 6px 0' }}>
      Cybermarket tokens
    </p>
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <p className="token-price" style={{ marginRight: 12 }}>
        200.44
      </p>
      <p className="some-white-text">equals 230 222.00 USD</p>
    </div>
    <div style={{ display: 'flex' }}>
      <div style={{ marginRight: 60 }}>
        <div
          style={{
            display: 'flex',
            margin: '40px 0 6px 0',
          }}
        >
          <img src={ethLogo} alt="ETH" />
          <p className="pink-text" style={{ margin: 0, marginLeft: 10 }}>
            ETH
          </p>
        </div>
        <p className="token-price">200.2444</p>
      </div>
      <div style={{ marginRight: 60 }}>
        <div
          style={{
            display: 'flex',
            margin: '40px 0 10px 0',
          }}
        >
          <img src={btcLogo} alt="BTC" />
          <p className="pink-text" style={{ margin: 0, marginLeft: 10 }}>
            BTC
          </p>
        </div>
        <p className="token-price">6.234 2323</p>
      </div>
      <div style={{ marginRight: 60 }}>
        <div
          style={{
            display: 'flex',
            margin: '40px 0 10px 0',
          }}
        >
          <img src={usdtLogo} alt="USDT" />
          <p className="pink-text" style={{ margin: 0, marginLeft: 10 }}>
            USDT
          </p>
        </div>
        <p className="token-price">1.344</p>
      </div>
      <div style={{ marginRight: 60 }}>
        <div
          style={{
            display: 'flex',
            margin: '40px 0 10px 0',
          }}
        >
          <img src={usdcLogo} alt="USDC" />
          <p className="pink-text" style={{ margin: 0, marginLeft: 10 }}>
            USDC
          </p>
        </div>
        <p className="token-price">3.34444</p>
      </div>
    </div>
  </div>
)

export default TotalBalance
