import React from 'react'

import '../MyZone.css'
import background from '../../../assets/raster/admin-background.png'
import arrowRight from '../../../assets/vector/right-pink-arrow.svg'

import identityIcon from '../../../assets/vector/identity-icon.svg'
import {
  BuyTokenPanel,
  CountDown,
  NavBar,
  TextInput,
} from '../../../components'

const Dashboard = () => (
  <div
    className="admin-screen"
    style={{ backgroundImage: `url(${background})` }}
  >
    <NavBar isSignedIn isExtended />

    <div className="admin-panel">
      <div className="admin-panel-left">
        <BuyTokenPanel />
      </div>
      <div className="admin-panel-right">
        <div className="profile-status">
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <p id="token-sales-live">Token sales is LIVE</p>
            <p className="white-text" style={{ marginLeft: 32 }}>
              Current bonus <span style={{ color: '#54F7C5' }}>18%</span>
            </p>
          </div>

          <CountDown dateToCountDown={new Date(2021, 4, 31)} />
        </div>

        <div className="identity-verification">
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <img src={identityIcon} alt="Identity" />
            <h2
              className="admin-panel-title"
              style={{ marginBottom: 0, marginLeft: 20 }}
            >
              YOUR IDENTITY IS BEING REVIEWING...
            </h2>
          </div>
          <p
            className="lime-text"
            style={{ fontWeight: 500, margin: '30px 0 0 0' }}
          >
            This process usually takes some time. You will be informed through
            the email when we finalize verification.
          </p>
        </div>

        <div className="tips">
          <h2 className="admin-panel-title" style={{ marginBottom: 40 }}>
            JUST 3 STEPS TO GAIN A FULL POTENTIAL OF INVESTMENT!
          </h2>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p
              className="pink-text"
              style={{ flexGrow: 1, flexBasis: 0, margin: 0 }}
            >
              Verify your account to get access to all limited offers
            </p>
            <p
              className="pink-text"
              style={{ flexGrow: 1, flexBasis: 0, margin: '0 40px' }}
            >
              Set up your cryptocurrency wallet where bought tokens will be sent
            </p>
            <p
              className="pink-text"
              style={{ flexGrow: 1, flexBasis: 0, margin: 0 }}
            >
              Buy tokens with Bitcoin, Ethereum, USDC or USDT
            </p>
          </div>
        </div>

        <div className="profit-counter">
          <h2 className="admin-panel-title" style={{ marginBottom: 40 }}>
            HOW MUCH YOU WILL PROFIT?
          </h2>

          <div style={{ display: 'flex', alignItems: 'center' }}>
            <TextInput
              title="IF YOU INVEST TODAY"
              insertion="USD"
              value="50.00"
              styleId={2}
              additionalStyle={{ width: '100%' }}
              type="number"
            />
            <img src={arrowRight} alt="Arrow" style={{ margin: '0 54px' }} />
            <TextInput
              title="YOUR PROFIT AT 5TH STAGE"
              insertion="USD"
              value="50.00"
              styleId={2}
              additionalStyle={{ width: '100%' }}
              type="number"
            />
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Dashboard
