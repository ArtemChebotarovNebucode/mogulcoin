import React, { useState } from 'react'

import './Register.css'
import background from '../../assets/raster/header-background.png'
import logo from '../../assets/vector/logo.svg'
import { PrimaryButton, TextInput } from '../../components'
import rightArrow from '../../assets/vector/right-lime-arrow.svg'
import CheckBoxInput from '../../components/CheckBoxInput/CheckBoxInput'
import floorTint from '../../assets/raster/floor-tint.png'

const Register = () => {
  const [isPrivacyPolicyChecked, setIsPrivacyPolicyChecked] = useState(false)
  const [isServiceTermsChecked, setIsServiceTermsChecked] = useState(false)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [passwordConfirm, setPasswordConfirm] = useState('')
  const [emailValidity, setEmailValidity] = useState(null)
  const [passwordValidity, setPasswordValidity] = useState(null)
  const [passwordConfirmValidity, setPasswordConfirmValidity] = useState(null)

  const isEmail = (_email) => {
    // eslint-disable-next-line no-useless-escape
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    return re.test(String(_email).toLowerCase())
  }

  const handleOnSubmit = (e) => {
    e.preventDefault()

    if (!isEmail(email)) setEmailValidity('Invalid email address')
    else setEmailValidity(null)

    if (password === '') setPasswordValidity('Invalid password')
    else setPasswordValidity(null)

    if (passwordConfirm !== password)
      setPasswordConfirmValidity('Password mismatch')
    else setPasswordConfirmValidity(null)
  }

  return (
    <div
      className="register-screen"
      style={{ backgroundImage: `url(${background})`, display: 'flex' }}
    >
      <div
        className="animated-floor"
        style={{ height: 647, top: 'auto', bottom: 0 }}
      />
      <img
        src={floorTint}
        alt="Tint"
        style={{
          position: 'absolute',
          bottom: 0,
          height: 330,
          width: '100%',
        }}
      />

      <div className="register-panel">
        <div>
          <img src={logo} alt="Logo" />
          <h2
            className="title-big"
            style={{ margin: '80px 0 0 0', width: '70%', textAlign: 'left' }}
          >
            Join the revolution!
          </h2>
          <h3
            className="pink-text"
            style={{ margin: '20px 0 80px 0', fontSize: 24 }}
          >
            Instantly. Safe. Reliable.
          </h3>

          <form onSubmit={handleOnSubmit}>
            <TextInput
              placeholder="E-mail address"
              styleId={4}
              onChange={(e) => setEmail(e.target.value)}
              additionalStyle={{ margin: '0 0 5px 0' }}
            />

            <p className="pink-text" style={{ margin: '0 0 40px 0' }}>
              {emailValidity}
            </p>

            <TextInput
              placeholder="Password"
              styleId={4}
              additionalStyle={{ margin: '40px 0 0 0' }}
              onChange={(e) => setPassword(e.target.value)}
            />

            <p className="pink-text" style={{ margin: '0 0 40px 0' }}>
              {passwordValidity}
            </p>

            <TextInput
              placeholder="Confirm password"
              styleId={4}
              additionalStyle={{ margin: '40px 0 0 0' }}
              onChange={(e) => setPasswordConfirm(e.target.value)}
            />

            <p className="pink-text" style={{ margin: '0 0 40px 0' }}>
              {passwordConfirmValidity}
            </p>

            <CheckBoxInput
              checked={isServiceTermsChecked}
              handleOnCheck={(isChecked) => {
                setIsServiceTermsChecked(isChecked)
              }}
              label={
                <>
                  I agree with&nbsp;
                  <a
                    href="/#"
                    style={{ color: 'white', textDecoration: 'underline' }}
                  >
                    Terms of Service
                  </a>
                </>
              }
              id="service-terms"
              additionalStyles={{ marginBottom: 20 }}
            />

            <CheckBoxInput
              label={
                <>
                  I agree with&nbsp;
                  <a
                    href="/#"
                    style={{ color: 'white', textDecoration: 'underline' }}
                  >
                    Privacy Policy
                  </a>
                </>
              }
              checked={isPrivacyPolicyChecked}
              handleOnCheck={(isChecked) =>
                setIsPrivacyPolicyChecked(isChecked)
              }
              id="privacy-policy"
            />

            <div
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'flex-end',
                marginTop: 30,
              }}
            >
              <PrimaryButton
                icon={rightArrow}
                textContent="SIGN UP"
                type="submit"
              />
            </div>
          </form>
        </div>
        <p className="bottom-sign" style={{ marginTop: 80 }}>
          Already have an account?{' '}
          <a
            href="/login"
            style={{ color: '#D10286', textDecoration: 'underline' }}
          >
            Sign in
          </a>
        </p>
      </div>
      <div className="register-greeting">
        <h1 className="title-huge">MAJOR ICO EVENT of 2021</h1>
        <h3 className="subtitle-lime">
          Welcome to the hottest hub for the streaming community
        </h3>
        <h4 className="subtitle-white">
          introducing the MOGUL NFT Marketplace
        </h4>
      </div>
    </div>
  )
}

export default Register
