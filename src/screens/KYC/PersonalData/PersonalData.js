import React, { useState } from 'react'

import './PersonalData.css'
import PropTypes from 'prop-types'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  DropDown,
  TextInput,
} from '../../../components'

import countries from '../../../data/countries'
import nationalities from '../../../data/nationalities'
import CheckBoxInput from '../../../components/CheckBoxInput/CheckBoxInput'

const PersonalData = ({ setCurrentStep }) => {
  const [isTaxResident, setIsTaxResident] = useState(false)
  const [
    hasProminentPoliticalPosition,
    setHasProminentPoliticalPosition,
  ] = useState(false)

  const [birthDate, setBirthDate] = useState('')
  const [name, setName] = useState('')
  const [surname, setSurname] = useState('')
  const [sex, setSex] = useState('')
  const [countryOfBirth, setCountryOfBirth] = useState('')
  const [taxCountry, setTaxCountry] = useState('')
  const [nationality, setNationality] = useState('')
  const [incomeSource, setIncomeSource] = useState('')

  const [nameValidity, setNameValidity] = useState(null)
  const [surnameValidity, setSurnameValidity] = useState(null)
  const [birthDateValidity, setBirthDateValidity] = useState(null)
  const [sexValidity, setSexValidity] = useState(null)
  const [countryOfBirthValidity, setCountryOfBirthValidity] = useState(null)
  const [nationalityValidity, setNationalityValidity] = useState(null)
  const [taxCountryValidity, setTaxCountryValidity] = useState(null)
  const [incomeSourceValidity, setIncomeSourceValidity] = useState(null)

  const checkValidity = () => {
    let valid = true

    if (birthDate === '') {
      setBirthDateValidity('Invalid birth date')
      valid = false
    } else {
      setBirthDateValidity(null)
    }

    if (name === '') {
      setNameValidity('Invalid name')
      valid = false
    } else {
      setNameValidity(null)
    }

    if (surname === '') {
      setSurnameValidity('Invalid surname')
      valid = false
    } else {
      setSurnameValidity(null)
    }

    if (sex === '') {
      setSexValidity('Invalid sex')
      valid = false
    } else {
      setSexValidity(null)
    }

    if (countryOfBirth === '') {
      setCountryOfBirthValidity('Invalid country of birth')
      valid = false
    } else {
      setCountryOfBirthValidity(null)
    }

    if (taxCountry === '') {
      setTaxCountryValidity('Invalid country of birth')
      valid = false
    } else {
      setTaxCountryValidity(null)
    }

    if (nationality === '') {
      setNationalityValidity('Invalid nationality')
      valid = false
    } else {
      setNationalityValidity(null)
    }

    if (incomeSource === '') {
      setIncomeSourceValidity('Invalid income source')
      valid = false
    } else {
      setIncomeSourceValidity(null)
    }

    return valid
  }

  return (
    <>
      <AdminPanelSteps
        steps={[
          'Personal data',
          'Address and Contact',
          'Legal source income',
          'ID card verification',
        ]}
        activeStepIndex={0}
        additionalStyles={{ padding: 0 }}
      />

      <div className="personal-data">
        <p className="admin-panel-title">Personal data</p>
        <p className="lime-text" style={{ margin: '40px 0' }}>
          Verify your account to get access to all limited offers.
        </p>

        <div className="personal-data-inputs">
          <div>
            <TextInput
              styleId={4}
              additionalStyle={{ margin: 0 }}
              title="NAME"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            {nameValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {nameValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              styleId={4}
              additionalStyle={{ margin: 0 }}
              title="SURNAME"
              value={surname}
              onChange={(e) => setSurname(e.target.value)}
            />
            {surnameValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {surnameValidity}
              </p>
            ) : null}
          </div>

          <div>
            <DropDown
              type="date-picker"
              additionalStyle={{ margin: 0 }}
              title="BIRTH DATE"
              maxDate={new Date()}
              startDate={birthDate}
              setStartDate={setBirthDate}
              value={birthDate}
              onChange={(e) => setBirthDate(e.target.value)}
            />
            {birthDateValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {birthDateValidity}
              </p>
            ) : null}
          </div>

          <div>
            <DropDown
              type="single-choice"
              title="SEX"
              options={['Male', 'Female']}
              onChange={(value) => {
                setSex(value)
              }}
            />
            {sexValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {sexValidity}
              </p>
            ) : null}
          </div>

          <div>
            <DropDown
              type="single-choice"
              options={countries.map((country) => country.name)}
              title="COUNTRY OF BIRTH"
              onChange={(value) => setCountryOfBirth(value)}
            />
            {countryOfBirthValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {countryOfBirthValidity}
              </p>
            ) : null}
          </div>

          <div>
            <DropDown
              type="single-choice"
              options={nationalities}
              title="NATIONALITY"
              onChange={(value) => setNationality(value)}
            />
            {nameValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {nationalityValidity}
              </p>
            ) : null}
          </div>

          <div>
            <DropDown
              type="single-choice"
              options={countries.map((country) => country.name)}
              title="COUNTRY OF TAX RESIDENCE"
              onChange={(value) => setTaxCountry(value)}
            />
            {taxCountryValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {taxCountryValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              styleId={4}
              additionalStyle={{ margin: 0 }}
              title="FROM WHERE ARE YOUR SOURCES INCOME?"
              value={incomeSource}
              onChange={(e) => setIncomeSource(e.target.value)}
            />
            {incomeSourceValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {incomeSourceValidity}
              </p>
            ) : null}
          </div>
        </div>

        <CheckBoxInput
          label={<p className="checkbox-label">I am tax resident of USA</p>}
          isChecked={isTaxResident}
          handleOnCheck={() => setIsTaxResident(!isTaxResident)}
          id="isTaxResident"
        />
        <CheckBoxInput
          label={
            <p className="checkbox-label">
              I am a person on a prominent political position
            </p>
          }
          isChecked={hasProminentPoliticalPosition}
          handleOnCheck={() =>
            setHasProminentPoliticalPosition(!hasProminentPoliticalPosition)
          }
          id="hasProminentPoliticalPosition"
        />
      </div>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'Next',
            onClick: () => {
              if (checkValidity()) setCurrentStep(1)
            },
          },
        ]}
        buttonsLeft={[
          {
            label: 'Back',
            onClick: () => {
              window.location.href = '/dashboard'
            },
          },
        ]}
      />
    </>
  )
}
export default PersonalData

PersonalData.propTypes = {
  setCurrentStep: PropTypes.func,
}
