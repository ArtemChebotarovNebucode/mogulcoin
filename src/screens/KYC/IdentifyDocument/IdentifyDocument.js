import React, { useState } from 'react'

import './IdentifyDocument.css'

import PropTypes from 'prop-types'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  DropDown,
  FileInput,
  TextInput,
} from '../../../components'
import CheckBoxInput from '../../../components/CheckBoxInput/CheckBoxInput'

const IdentifyDocument = ({ setCurrentStep }) => {
  const [isTransferAuthorized, setIsTransferAuthorized] = useState(false)
  const [dateOfIssue, setDateOfIssue] = useState('')
  const [dateOfExpiry, setDateOfExpiry] = useState('')
  const [documentType, setDocumentType] = useState('')
  const [documentNumber, setDocumentNumber] = useState('')
  const [documentPhotos, setDocumentPhotos] = useState([])
  const [selfie, setSelfie] = useState([])

  const [dateOfIssueValidity, setDateOfIssueValidity] = useState(null)
  const [dateOfExpiryValidity, setDateOfExpiryValidity] = useState(null)
  const [documentTypeValidity, setDocumentTypeValidity] = useState(null)
  const [documentNumberValidity, setDocumentNumberValidity] = useState(null)
  const [documentPhotosValidity, setDocumentPhotosValidity] = useState(false)
  const [selfieValidity, setSelfieValidity] = useState(false)
  const [
    isTransferAuthorizedValidity,
    setIsTransferAuthorizedValidity,
  ] = useState(false)

  const checkValidity = () => {
    let valid = true

    if (dateOfIssue === '') {
      setDateOfIssueValidity('Invalid date of issue')
      valid = false
    } else {
      setDateOfIssueValidity(null)
    }

    if (dateOfExpiry === '') {
      setDateOfExpiryValidity('Invalid date of expiry')
      valid = false
    } else {
      setDateOfExpiryValidity(null)
    }

    if (documentType === '') {
      setDocumentTypeValidity('Invalid document type')
      valid = false
    } else {
      setDocumentTypeValidity(null)
    }

    if (documentNumber === '') {
      setDocumentNumberValidity('Invalid document number')
      valid = false
    } else {
      setDocumentNumberValidity(null)
    }

    if (documentPhotos.length === 0) {
      setDocumentPhotosValidity(true)
      valid = false
    } else {
      setDocumentPhotosValidity(false)
    }

    if (selfie.length === 0) {
      setSelfieValidity(true)
      valid = false
    } else {
      setSelfieValidity(false)
    }

    if (!isTransferAuthorized) {
      setIsTransferAuthorizedValidity(true)
      valid = false
    } else {
      setIsTransferAuthorizedValidity(false)
    }

    return valid
  }

  return (
    <>
      <AdminPanelSteps
        steps={[
          'Personal data',
          'Address and Contact',
          'Legal source income',
          'ID card verification',
        ]}
        activeStepIndex={3}
        additionalStyles={{ padding: 0 }}
      />

      <div className="identify-document">
        <p className="admin-panel-title">my identify document</p>
        <p className="lime-text" style={{ margin: '40px 0' }}>
          Verify your account to get access to all limited offers.
        </p>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            gridTemplateRows: '1fr 1fr',
            gap: '20px 20px',

            alignItems: 'center',
          }}
        >
          <div>
            <DropDown
              type="single-choice"
              options={['Identity card', 'Passport']}
              title="DOCUMENT TYPE"
              value={documentType}
              onChange={(option) => setDocumentType(option)}
            />
            {documentTypeValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {documentTypeValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              title="DOCUMENT NUMBER"
              styleId={4}
              additionalStyle={{ margin: 0 }}
              value={documentNumber}
              onChange={(e) => setDocumentNumber(e.target.value)}
            />
            {documentNumberValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {documentNumberValidity}
              </p>
            ) : null}
          </div>

          <div>
            <DropDown
              type="date-picker"
              startDate={dateOfIssue}
              setStartDate={setDateOfIssue}
              title="DATE OF ISSUE"
              maxDate={new Date()}
            />
            {dateOfIssueValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {dateOfIssueValidity}
              </p>
            ) : null}
          </div>

          <div>
            <DropDown
              type="date-picker"
              startDate={dateOfExpiry}
              setStartDate={setDateOfExpiry}
              title="DATE OF EXPIRY"
              minDate={new Date()}
            />
            {dateOfExpiryValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {dateOfExpiryValidity}
              </p>
            ) : null}
          </div>
        </div>

        <p className="lime-text" style={{ margin: '80px 0 40px 0' }}>
          Send photos of both sides of your document to complete registration
        </p>

        <div style={{ height: 200 }}>
          <FileInput
            title="Scan or photo of a document"
            subtitle="(max. 5mb file size; only jpg, png)"
            instruction={
              <p style={{ marginBottom: 0 }}>
                Drag a Document Scan or Photo or{' '}
                <span style={{ textDecoration: 'underline', fontWeight: 600 }}>
                  Select a file
                </span>
              </p>
            }
            multiple
            setFiles={setDocumentPhotos}
            invalid={documentPhotosValidity}
            files={documentPhotos}
          />
        </div>

        <p className="lime-text" style={{ margin: '80px 0 22px 0' }}>
          Send photo (selfie) of yourself with a document you have selected for
          a verification
        </p>
        <p className="white-text" style={{ margin: 0 }}>
          We encourage to add to a photo handwritten note “Only to registration
          at Value” with today’s date and your signature. You can hold both
          documents in one hand or both, but try to not hide face by them.
        </p>

        <div style={{ height: 200, margin: '40px 0 30px 0' }}>
          <FileInput
            title="Scan or photo of a document"
            subtitle="(max. 5mb file size; only jpg, png)"
            instruction={
              <p style={{ marginBottom: 0 }}>
                Drag a Document Scan or Photo or{' '}
                <span style={{ textDecoration: 'underline', fontWeight: 600 }}>
                  Select a file
                </span>
              </p>
            }
            multiple
            invalid={selfieValidity}
            files={selfie}
            setFiles={setSelfie}
          />
        </div>
        <CheckBoxInput
          label={
            <p className="checkbox-label">
              I authorize the transfer of personal data provided in the form to
              third parties for the purpose of verification and checking the
              profile in the context of criminal records
            </p>
          }
          isChecked={isTransferAuthorized}
          handleOnCheck={() => setIsTransferAuthorized(!isTransferAuthorized)}
          id="isTransferAuthorized"
          invalid={isTransferAuthorizedValidity}
        />
      </div>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'Next',
            onClick: (e) => {
              if (!checkValidity()) e.preventDefault()
            },
            type: 'submit',
          },
        ]}
        buttonsLeft={[{ label: 'Back', onClick: () => setCurrentStep(2) }]}
      />
    </>
  )
}
export default IdentifyDocument

IdentifyDocument.propTypes = {
  setCurrentStep: PropTypes.func,
}
