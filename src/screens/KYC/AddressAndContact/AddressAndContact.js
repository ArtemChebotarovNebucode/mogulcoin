import React, { useState } from 'react'

import './AddressAndContact.css'
import PropTypes from 'prop-types'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  TextInput,
} from '../../../components'

const AddressAndContact = ({ setCurrentStep }) => {
  const [postalCode, setPostalCode] = useState('')
  const [city, setCity] = useState('')
  const [street, setStreet] = useState('')
  const [buildNo, setBuildNo] = useState('')
  const [apartNo, setApartNo] = useState('')
  const [phone, setPhone] = useState('')

  const [postalCodeValidity, setPostalCodeValidity] = useState(null)
  const [cityValidity, setCityValidity] = useState(null)
  const [streetValidity, setStreetValidity] = useState(null)
  const [buildNoValidity, setBuildNoValidity] = useState(null)
  const [apartNoValidity, setApartNoValidity] = useState(null)
  const [phoneValidity, setPhoneValidity] = useState(null)

  const checkValidity = () => {
    let valid = true

    if (postalCode === '') {
      setPostalCodeValidity('Invalid postal code')
      valid = false
    } else {
      setPostalCodeValidity(null)
    }

    if (city === '') {
      setCityValidity('Invalid city name')
      valid = false
    } else {
      setCityValidity(null)
    }

    if (street === '') {
      setStreetValidity('Invalid street name')
      valid = false
    } else {
      setStreetValidity(null)
    }

    if (buildNo === '') {
      setBuildNoValidity('Invalid building number')
      valid = false
    } else {
      setBuildNoValidity(null)
    }

    if (apartNo === '') {
      setApartNoValidity('Invalid apartment number')
      valid = false
    } else {
      setApartNoValidity(null)
    }

    if (phone === '') {
      setPhoneValidity('Invalid phone number')
      valid = false
    } else {
      setPhoneValidity(null)
    }

    return valid
  }

  return (
    <>
      <AdminPanelSteps
        steps={[
          'Personal data',
          'Address and Contact',
          'Legal source income',
          'ID card verification',
        ]}
        activeStepIndex={1}
        additionalStyles={{ padding: 0 }}
      />
      <div className="address-and-contact">
        <p className="admin-panel-title">address and contact</p>
        <p className="lime-text" style={{ margin: '40px 0' }}>
          Verify your account to get access to all limited offers.
        </p>
        <div className="double-inputs-container">
          <div>
            <TextInput
              styleId={4}
              title="POSTAL CODE"
              additionalStyle={{ margin: 0 }}
              value={postalCode}
              onChange={(e) => setPostalCode(e.target.value)}
            />
            {postalCodeValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {postalCodeValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              styleId={4}
              title="CITY"
              additionalStyle={{ margin: 0 }}
              value={city}
              onChange={(e) => setCity(e.target.value)}
            />
            {cityValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {cityValidity}
              </p>
            ) : null}
          </div>
        </div>

        <div style={{ margin: '0 0 30px 0' }}>
          <TextInput
            styleId={4}
            title="STREET"
            value={street}
            onChange={(e) => setStreet(e.target.value)}
            additionalStyle={{ margin: '31px 0 0 0' }}
          />
          {streetValidity ? (
            <p className="pink-text" style={{ margin: 0 }}>
              {streetValidity}
            </p>
          ) : null}
        </div>

        <div className="double-inputs-container">
          <div>
            <TextInput
              styleId={4}
              title="BUILD. NO"
              additionalStyle={{ margin: 0 }}
              value={buildNo}
              onChange={(e) => setBuildNo(e.target.value)}
            />
            {buildNoValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {buildNoValidity}
              </p>
            ) : null}
          </div>

          <div>
            <TextInput
              styleId={4}
              title="APART. NO"
              additionalStyle={{ margin: 0 }}
              value={apartNo}
              onChange={(e) => setApartNo(e.target.value)}
            />
            {apartNoValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {apartNoValidity}
              </p>
            ) : null}
          </div>
        </div>

        <div style={{ margin: '0 0 30px 0' }}>
          <TextInput
            styleId={4}
            title="PHONE"
            value={phone}
            isPhoneNumber
            onChange={(value) => setPhone(value)}
            additionalStyle={{ margin: '31px 0 0 0' }}
          />
          {phoneValidity ? (
            <p className="pink-text" style={{ margin: 0 }}>
              {phoneValidity}
            </p>
          ) : null}
        </div>
      </div>
      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'Next',
            onClick: () => {
              if (checkValidity()) setCurrentStep(2)
            },
          },
        ]}
        buttonsLeft={[{ label: 'Back', onClick: () => setCurrentStep(0) }]}
      />
    </>
  )
}

export default AddressAndContact

AddressAndContact.propTypes = {
  setCurrentStep: PropTypes.func,
}
