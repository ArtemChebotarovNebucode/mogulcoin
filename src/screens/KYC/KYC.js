import React, { useState } from 'react'

import { NavBar } from '../../components'
import background from '../../assets/raster/admin-background.png'
import PersonalData from './PersonalData/PersonalData'
import AddressAndContact from './AddressAndContact/AddressAndContact'
import LegalSourceIncome from './LegalSourceIncome/LegalSourceIncome'
import IdentifyDocument from './IdentifyDocument/IdentifyDocument'

const KYC = () => {
  const [currentStep, setCurrentStep] = useState(0)

  return (
    <form
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <NavBar isSignedIn isExtended />

      {currentStep === 0 ? (
        <PersonalData setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 1 ? (
        <AddressAndContact setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 2 ? (
        <LegalSourceIncome setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 3 ? (
        <IdentifyDocument setCurrentStep={setCurrentStep} />
      ) : null}
    </form>
  )
}

export default KYC
