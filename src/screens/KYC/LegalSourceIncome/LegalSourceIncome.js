import React, { useState } from 'react'

import './LegalSourceIncome.css'
import PropTypes from 'prop-types'
import { AdminPanelNavigation, AdminPanelSteps } from '../../../components'
import CheckBoxInput from '../../../components/CheckBoxInput/CheckBoxInput'

const LegalSourceIncome = ({ setCurrentStep }) => {
  const [isYesChecked, setIsYesChecked] = useState(true)
  const [isNotCriminal, setIsNotCriminal] = useState(false)
  const [willUpdateStatement, setWillUpdateStatement] = useState(false)

  const [isYesCheckedInvalid, setIsYesCheckedInvalid] = useState(false)
  const [isNotCriminalInvalid, setIsNotCriminalInvalid] = useState(false)
  const [willUpdateStatementInvalid, setWillUpdateStatementInvalid] = useState(
    false,
  )

  const checkValidity = () => {
    let valid = true

    if (!isYesChecked) {
      setIsYesCheckedInvalid(true)
      valid = false
    } else {
      setIsYesCheckedInvalid(false)
    }

    if (!isNotCriminal) {
      setIsNotCriminalInvalid(true)
      valid = false
    } else {
      setIsNotCriminalInvalid(false)
    }

    if (!willUpdateStatement) {
      setWillUpdateStatementInvalid(true)
      valid = false
    } else {
      setWillUpdateStatementInvalid(false)
    }

    return valid
  }

  return (
    <>
      <AdminPanelSteps
        steps={[
          'Personal data',
          'Address and Contact',
          'Legal source income',
          'ID card verification',
        ]}
        activeStepIndex={2}
        additionalStyles={{ padding: 0 }}
      />

      <div className="legal-source-income">
        <p className="admin-panel-title">LEGAL SOURCE INCOME</p>
        <p className="lime-text" style={{ margin: '40px 0 80px 0' }}>
          Verify your account to get access to all limited offers.
        </p>
        <p className="white-text" style={{ margin: 0 }}>
          1. I declare that the funds deposited from my bank accounts to the
          Website come from fully legal sources and the use of the Website is
          not intended to launder money or finance terrorism within the meaning
          of applicable law.
        </p>
        <p className="white-text" style={{ margin: '30px 0' }}>
          2. I declare none of my assets, net worth, or current or future income
          results from drug trafficking, money laundering, or any other
          activities that are illegal in the European Union or in any other
          jurisdiction.
        </p>
        <p className="white-text" style={{ margin: '0 0 20px 0' }}>
          3. On the day of signing this statement, all the above information and
          statements are true and correct.
        </p>
        <div
          style={{
            display: 'flex',
            width: '15%',
            justifyContent: 'space-between',
          }}
        >
          <CheckBoxInput
            label={<p className="checkbox-label">Yes</p>}
            isChecked={isYesChecked}
            handleOnCheck={() => setIsYesChecked(true)}
            id="isYesOptionChecked"
            additionalStyles={{ marginRight: 40 }}
            invalid={isYesCheckedInvalid}
          />

          <CheckBoxInput
            label={<p className="checkbox-label">No</p>}
            isChecked={!isYesChecked}
            handleOnCheck={() => setIsYesChecked(false)}
            id="isNoOptionChecked"
          />
        </div>

        <hr
          style={{ borderColor: '#736982', opacity: 0.3, margin: '30px 0' }}
        />

        <CheckBoxInput
          label={
            <p className="checkbox-label">
              I am aware of criminal responsibility for submission a fake
              statement and delivering fake data
            </p>
          }
          isChecked={isNotCriminal}
          handleOnCheck={() => setIsNotCriminal(!isNotCriminal)}
          id="isNotCriminal"
          invalid={isNotCriminalInvalid}
        />

        <CheckBoxInput
          label={
            <p className="checkbox-label">
              I undertake to update the statement regarding the client&apos;s
              status in the event of a change in circumstances causing the
              previous statement to become invalid.
            </p>
          }
          isChecked={willUpdateStatement}
          handleOnCheck={() => setWillUpdateStatement(!willUpdateStatement)}
          id="willUpdateStatement"
          invalid={willUpdateStatementInvalid}
        />
      </div>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'Next',
            onClick: () => {
              if (checkValidity()) setCurrentStep(3)
            },
          },
        ]}
        buttonsLeft={[{ label: 'Back', onClick: () => setCurrentStep(1) }]}
      />
    </>
  )
}

export default LegalSourceIncome

LegalSourceIncome.propTypes = {
  setCurrentStep: PropTypes.func,
}
