import React, { useState } from 'react'

import './PayWithOtherWallet.css'
import PropTypes from 'prop-types'
import { AdminPanelNavigation, AdminPanelSteps } from '../../../components'
import payQR from '../../../assets/vector/pay-qr.svg'
import copyIcon from '../../../assets/vector/copy-pink-icon.svg'

const PayWithOtherWallet = ({ setCurrentStep }) => {
  // eslint-disable-next-line no-unused-vars
  const [uniqueWalletId, setUniqueWalletId] = useState(
    '1ETs5DyjEt2g9XWxQX34oy3NVVxBfVZc5p',
  )

  return (
    <>
      <AdminPanelSteps steps={['Choose wallet', 'Pay']} activeStepIndex={1} />

      <div className="pay-with-other-wallet">
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ width: '100%' }}>
            <p className="admin-panel-title">PAY WITH OTHER WALLET</p>
            <p className="lime-text" style={{ margin: '40px 0' }}>
              In your provider wallet, order a transfer to a below unique wallet
              ID.
            </p>

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <p className="pink-text" style={{ width: '25%', margin: 0 }}>
                Total amount
              </p>
              <p className="amount" style={{ margin: 0 }}>
                0.016117827 <span className="currency-name">BTC</span>
              </p>
            </div>

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                margin: '40px 0',
              }}
            >
              <p className="pink-text" style={{ width: '25%', margin: 0 }}>
                Unique Wallet ID:
              </p>
              <p className="white-text" style={{ marginLeft: 0, margin: 0 }}>
                {uniqueWalletId}
              </p>
              <button
                type="button"
                onClick={() => navigator.clipboard.writeText(uniqueWalletId)}
                className="button-wrapper"
              >
                <img
                  src={copyIcon}
                  alt="Copy"
                  style={{ cursor: 'pointer', marginLeft: 12 }}
                />
              </button>
            </div>

            <p className="lime-text" style={{ margin: 0 }}>
              As soon as we register the transfer, the tokens will be in your
              wallet:
            </p>

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                margin: '40px 0',
              }}
            >
              <p className="pink-text" style={{ width: '25%', margin: 0 }}>
                Your wallet
              </p>
              <p className="white-text" style={{ margin: 0 }}>
                0x470062676578fd61342D062787EA9CA8CFF3E769
              </p>
            </div>
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <p className="pink-text" style={{ width: '25%', margin: 0 }}>
                Order expires at
              </p>
              <p className="white-text" style={{ margin: 0 }}>
                23 Feb 2021, 02:42
              </p>
            </div>
          </div>

          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <img src={payQR} alt="QR" />
            <p
              className="lime-text"
              style={{ margin: 0, fontSize: 12, marginTop: 10 }}
            >
              Scan QR code and pay faster
            </p>
          </div>
        </div>
      </div>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'Next',
            onClick: () => {
              setCurrentStep(2)
            },
          },
        ]}
        buttonsLeft={[
          {
            label: 'Back',
            onClick: () => setCurrentStep(0),
          },
        ]}
      />
    </>
  )
}
export default PayWithOtherWallet

PayWithOtherWallet.propTypes = {
  setCurrentStep: PropTypes.func,
}
