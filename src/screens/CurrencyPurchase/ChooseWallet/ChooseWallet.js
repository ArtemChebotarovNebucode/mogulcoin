import React, { useState } from 'react'

import './ChooseWallet.css'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import {
  AccessMethod,
  AdminPanelNavigation,
  AdminPanelSteps,
} from '../../../components'

const ChooseWallet = ({ setCurrentStep }) => {
  const [chosenWallet, setChosenWallet] = useState({
    alreadyInstalled: true,
    other: false,
  })

  const history = useHistory()

  return (
    <>
      <AdminPanelSteps steps={['Choose wallet', 'Pay']} activeStepIndex={0} />

      <div className="choose-wallet">
        <p className="admin-panel-title">choose wallet</p>
        <p className="lime-text" style={{ margin: '40px 0 80px 0' }}>
          Which wallet do you want to use to proceed?
        </p>

        <div style={{ display: 'flex' }}>
          <AccessMethod
            text="ALREADY INSTALLED WALLET"
            isActive={chosenWallet.alreadyInstalled}
            handleOnClick={() =>
              setChosenWallet({
                alreadyInstalled: true,
                other: false,
              })
            }
            additionalStyles={{ marginRight: 40 }}
          />
          <AccessMethod
            text="other wallet"
            isActive={chosenWallet.other}
            handleOnClick={() =>
              setChosenWallet({
                alreadyInstalled: false,
                other: true,
              })
            }
          />
        </div>
      </div>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'Next',
            onClick: () => {
              if (chosenWallet.alreadyInstalled) {
                setCurrentStep(1)
              } else if (chosenWallet.other) {
                // setCurrentStep()
              }
            },
          },
        ]}
        buttonsLeft={[
          {
            label: 'Back',
            onClick: () => history.push('/payment/pay-with-provider'),
          },
        ]}
      />
    </>
  )
}

export default ChooseWallet

ChooseWallet.propTypes = {
  setCurrentStep: PropTypes.func,
}
