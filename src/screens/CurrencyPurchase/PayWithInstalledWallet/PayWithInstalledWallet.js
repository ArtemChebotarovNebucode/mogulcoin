import React from 'react'

import './PayWithInstalledWallet.css'
import PropTypes from 'prop-types'
import { AdminPanelNavigation, AdminPanelSteps } from '../../../components'

const PayWithInstalledWallet = ({ setCurrentStep }) => (
  <>
    <AdminPanelSteps steps={['Choose wallet', 'Pay']} activeStepIndex={1} />

    <div className="pay-with-installed-wallet">
      <p className="admin-panel-title">PAY WITH INSTALLED WALLET</p>
      <p className="lime-text" style={{ margin: '40px 0' }}>
        You are ordering a transfer from installed wallet.
      </p>

      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          marginBottom: 40,
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', marginRight: 60 }}>
          <p className="pink-text" style={{ margin: '0 20px 0 0' }}>
            You will get:
          </p>
          <p className="amount" style={{ margin: 0 }}>
            1 <span className="currency-name">MGC</span>
          </p>
        </div>

        <div style={{ display: 'flex', alignItems: 'center' }}>
          <p className="pink-text" style={{ margin: '0 20px 0 0' }}>
            You total pay:
          </p>
          <p className="amount" style={{ margin: 0 }}>
            0.016117827 <span className="currency-name">BTC</span>
          </p>
        </div>
      </div>

      <div style={{ display: 'flex' }}>
        <p className="pink-text" style={{ margin: 0, marginRight: 40 }}>
          Balance after transaction:
        </p>

        <div className="currency-rate">
          <p className="lime-text" style={{ margin: 0, textAlign: 'right' }}>
            Cybermarket tokens:
          </p>
          <p className="white-text" style={{ margin: 0 }}>
            200.44
          </p>
          <p className="lime-text" style={{ margin: 0, textAlign: 'right' }}>
            ETH:
          </p>
          <p className="white-text" style={{ margin: 0 }}>
            200.244
          </p>
          <p className="lime-text" style={{ margin: 0, textAlign: 'right' }}>
            BTC:
          </p>
          <p className="white-text" style={{ margin: 0 }}>
            6.2342323
          </p>
        </div>
      </div>

      <p className="lime-text" style={{ margin: '63px 0 40px 0' }}>
        As soon as we register the transfer, the tokens will be in your wallet:
      </p>

      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          marginBottom: 40,
        }}
      >
        <p className="pink-text" style={{ width: '25%', margin: 0 }}>
          Your wallet
        </p>
        <p className="white-text" style={{ margin: 0 }}>
          0x470062676578fd61342D062787EA9CA8CFF3E769
        </p>
      </div>

      <div
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <p className="pink-text" style={{ width: '25%', margin: 0 }}>
          Order expires at
        </p>
        <p className="white-text" style={{ margin: 0 }}>
          23 Feb 2021, 02:42
        </p>
      </div>
    </div>

    <AdminPanelNavigation
      buttonsRight={[
        {
          label: 'Next',
          onClick: () => {},
          type: 'submit',
        },
      ]}
      buttonsLeft={[
        {
          label: 'Back',
          onClick: () => setCurrentStep(1),
        },
      ]}
    />
  </>
)
export default PayWithInstalledWallet

PayWithInstalledWallet.propTypes = {
  setCurrentStep: PropTypes.func,
}
