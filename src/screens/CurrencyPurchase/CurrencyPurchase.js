import React, { useState } from 'react'

import { useHistory } from 'react-router-dom'
import { NavBar } from '../../components'
import background from '../../assets/raster/admin-background.png'
import ChooseWallet from './ChooseWallet/ChooseWallet'
import PayWithOtherWallet from './PayWithOtherWallet/PayWithOtherWallet'
import PayWithInstalledWallet from './PayWithInstalledWallet/PayWithInstalledWallet'

const CurrencyPurchase = () => {
  const [currentStep, setCurrentStep] = useState(0)

  const history = useHistory()

  return (
    <form
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      }}
      onSubmit={() => history.push('/dashboard/all-transactions')}
    >
      <NavBar isSignedIn isExtended />

      {currentStep === 0 ? (
        <ChooseWallet setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 1 ? (
        <PayWithOtherWallet setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 2 ? (
        <PayWithInstalledWallet setCurrentStep={setCurrentStep} />
      ) : null}
    </form>
  )
}

export default CurrencyPurchase
