import React, { useState } from 'react'

import './PayWithProvider.css'
import { useHistory } from 'react-router-dom'
import { AdminPanelNavigation, NavBar, TextInput } from '../../../components'
import background from '../../../assets/raster/admin-background.png'

import payQR from '../../../assets/vector/pay-qr.svg'

const PayWithProvider = () => {
  const history = useHistory()
  const [walletId, setWalletId] = useState('')

  return (
    <form
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      }}
      onSubmit={() => history.push('/dashboard/all-transactions')}
    >
      <NavBar isSignedIn />

      <div className="pay-with-provider">
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ width: '64%' }}>
            <p className="admin-panel-title">pay with your provider</p>
            <p className="lime-text" style={{ margin: '40px 0 80px 0' }}>
              In your provider wallet, order a transfer to a below unique wallet
              ID. As soon as we register the transfer, the tokens will be in
              your wallet.
            </p>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <img src={payQR} alt="QR" />
            <p
              className="lime-text"
              style={{ margin: 0, fontSize: 12, marginTop: 10 }}
            >
              Scan QR code and pay faster
            </p>
          </div>
        </div>

        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            marginBottom: 40,
          }}
        >
          <p className="pink-text" style={{ width: '20%', margin: 0 }}>
            Total amount
          </p>
          <p className="amount" style={{ margin: 0 }}>
            0.016117827 <span className="currency-name">BTC</span>
          </p>
        </div>

        <TextInput
          title="UNIQUE WALLET ID"
          isCopyingAllowed
          styleId={4}
          additionalStyle={{ margin: 0 }}
          value={walletId}
          onChange={(e) => setWalletId(e.target.value)}
        />

        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            margin: '40px 0',
          }}
        >
          <p className="pink-text" style={{ width: '20%', margin: 0 }}>
            Your wallet
          </p>
          <p className="white-text" style={{ margin: 0 }}>
            0x470062676578fd61342D062787EA9CA8CFF3E769
          </p>
        </div>

        <div
          style={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <p className="pink-text" style={{ width: '20%', margin: 0 }}>
            Order expires at
          </p>
          <p className="white-text" style={{ margin: 0 }}>
            23 Feb 2021, 02:42
          </p>
        </div>
      </div>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'Done',
            onClick: () => {},
            type: 'submit',
          },
        ]}
        buttonsLeft={[
          {
            label: 'Cancel',
            onClick: () => {
              history.push('/dashboard/all-transactions')
              window.location.href = '/dashboard/all-transactions'
            },
          },
        ]}
      />
    </form>
  )
}
export default PayWithProvider
