import React from 'react'

import '../../MainMarketing.css'

import slotMachine from '../../../../assets/raster/slot-machine.png'
import sun from '../../../../assets/raster/sun.png'
import gameBoy from '../../../../assets/raster/gameboy.png'
import consoles from '../../../../assets/raster/consoles.png'
import backgroundCircles from '../../../../assets/vector/background-circles.svg'
import { Footer, RecordsPanel, LikeMark, RoadMap } from '../../../../components'
import filipMartyni from '../../../../assets/raster/team-members/filip-martyni.jpg'
import philippSiemek from '../../../../assets/raster/team-members/ceo-photo.jpeg'
import linkedinLogo from '../../../../assets/vector/linkedin-logo.svg'
import dymitrLubczyk from '../../../../assets/raster/team-members/dymitr-lubczyk.png'
import animeCoinLogo from '../../../../assets/raster/animecoin.jpg'
import indahashLogo from '../../../../assets/raster/indahash.jpg'
import rallyLogo from '../../../../assets/raster/rally.jpg'
import instreamlyLogo from '../../../../assets/raster/instreamly.jpg'
import makerLogo from '../../../../assets/raster/maker.jpg'
import refereumLogo from '../../../../assets/raster/refereum.jpg'
import logoWrapped from '../../../../assets/vector/logo-wrapped.svg'

const Body = () => (
  <div className="intro">
    <div className="nft-intro">
      <h2 className="intro-title" style={{ margin: 0 }}>
        INTRODUCING THE MOGUL NFT MARKETPLACE
      </h2>

      <div className="nft-explained">
        <div className="text-section">
          <p className="white-text">
            Become a MOGUL in <span style={{ color: '#D10286' }}>every</span>{' '}
            sense of the word with our brand new NFT Marketplace!
          </p>
          <p className="white-text">
            Join exclusive streaming communities and participate in one of the
            first NFT Marketplaces for streamers in the world.
          </p>

          <div className="gradient-container">
            <p
              className="intro-title"
              style={{
                fontSize: 24,
                position: 'absolute',
                top: -30,
                margin: 0,
              }}
            >
              STREAMERS
            </p>
            <p className="pink-text" style={{ margin: 0 }}>
              can now generate a finite number of collectibles, in the form of
              either electronic artwork or in the form of special content
              videos, to be either sold or auctioned for MGUL – allowing
              streamers to crowdfund their newest projects.
            </p>
          </div>

          <p className="white-text">
            Anyone can place a bid for this exclusive content released by
            streamers – creating a unique addition to your collection of
            electronic artwork.
          </p>
          <p className="white-text" id="nft-text-bottom">
            On the MOGUL NFT Marketplace, you can not only find hidden gems, but
            also exchange them for other NFTs with your fellow streaming fans.
          </p>
          <p
            className="white-text"
            id="nft-text-bottom"
            style={{ marginBottom: 120 }}
          >
            <span style={{ color: '#D10286' }}>
              Become a true MOGUL by becoming a master at locating the best NFTs
              on the market
            </span>{' '}
            or simply enjoy your ever-growing art collection.
          </p>
        </div>

        <div className="pics-section">
          <img
            src={backgroundCircles}
            alt="Circles"
            style={{
              position: 'absolute',
              bottom: 0,
              right: 0,
            }}
            id="circles"
          />
          <img
            src={slotMachine}
            alt="Slot Machine"
            id="slot-machine"
            style={{
              zIndex: 1,
              position: 'absolute',
              bottom: 0,
              right: -100,
            }}
          />
        </div>
      </div>
    </div>

    <div className="become-mogul">
      <img src={sun} alt="Sun" id="sun" />

      <div className="become-mogul-content">
        <h2 className="intro-title" id="streaming-title">
          STREAMING IS ONE OF THE FASTEST GROWING AND MOST POPULAR TOOLS FOR
          CONTENT CREATORS WORLDWIDE!
        </h2>
      </div>
      <div className="become-mogul-content" id="become-mogul-content-text">
        <p className="pink-text" id="streaming-text">
          Don’t sleep on streaming – join an exploding{' '}
          <span style={{ fontWeight: 700, color: 'white' }}>
            $50 billion market
          </span>{' '}
          and become a MOGUL
        </p>
      </div>
    </div>

    <div className="why-mogul">
      <div className="why-mogul-title-rwd">
        <h2 className="why-mogul-title rwd">
          WHY{' '}
          <span
            style={{
              color: '#D10286',
              textDecoration: 'underline',
              margin: 0,
            }}
            className="blink"
          >
            MOGUL
          </span>
          ?
        </h2>
      </div>
      <div className="why-mogul-content">
        <div
          className="why-mogul-column title-column"
          style={{ flexGrow: 0.8, padding: '89px 20px 90px 0' }}
        >
          <div className="column-cell" style={{ justifyContent: 'flex-start' }}>
            <h2 className="why-mogul-title">
              WHY{' '}
              <span
                style={{
                  color: '#D10286',
                  textDecoration: 'underline',
                  margin: 0,
                }}
                className="blink"
              >
                MOGUL
              </span>
              ?
            </h2>
          </div>
          <div
            className="column-cell"
            style={{ marginTop: 30, justifyContent: 'flex-start' }}
          >
            <h5 className="feature-title">Blockchain technology</h5>
          </div>
          <div className="column-cell" style={{ justifyContent: 'flex-start' }}>
            <h5 className="feature-title">Connecting streamers and brands</h5>
          </div>
          <div className="column-cell" style={{ justifyContent: 'flex-start' }}>
            <h5 className="feature-title">NFT Marketplace</h5>
          </div>
          <div className="column-cell" style={{ justifyContent: 'flex-start' }}>
            <h5 className="feature-title">Facilitating ad placement</h5>
          </div>
          <div className="column-cell" style={{ justifyContent: 'flex-start' }}>
            <h5 className="feature-title">Customizable tokens</h5>
          </div>
        </div>
        <div className="why-mogul-column chosen">
          <div className="column-cell">
            <img
              src={`${logoWrapped}`}
              alt="Logo"
              className="title-cell"
              width={102}
              height={52}
            />
            {/* <h5 className="feature-title">MOGUL Coin</h5> */}
          </div>
          <div className="column-cell" style={{ marginTop: 30 }}>
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
        </div>
        <div
          className="why-mogul-column"
          style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
        >
          <div className="column-cell">
            <img
              src={`${animeCoinLogo}`}
              alt="Anime Coin"
              className="title-cell"
            />
            {/* <h5 className="feature-title">Anime Coin</h5> */}
          </div>
          <div className="column-cell" style={{ marginTop: 30 }}>
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
        </div>
        <div
          className="why-mogul-column"
          style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
        >
          <div className="column-cell">
            <img src={`${rallyLogo}`} alt="Rally" className="title-cell" />
            {/* <h5 className="feature-title">Rally</h5> */}
          </div>
          <div className="column-cell" style={{ marginTop: 30 }}>
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
        </div>
        <div
          className="why-mogul-column"
          style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
        >
          <div className="column-cell">
            <img
              src={`${refereumLogo}`}
              alt="Refereum"
              className="title-cell"
            />
            {/* <h5 className="feature-title">Refereum</h5> */}
          </div>
          <div className="column-cell" style={{ marginTop: 30 }}>
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
        </div>
        <div
          className="why-mogul-column"
          style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
        >
          <div className="column-cell">
            <img
              src={`${indahashLogo}`}
              alt="Inda Hash"
              className="title-cell"
            />
            {/* <h5 className="feature-title">indaHash</h5> */}
          </div>
          <div className="column-cell" style={{ marginTop: 30 }}>
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
        </div>
        <div
          className="why-mogul-column"
          style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
        >
          <div className="column-cell">
            <img src={`${makerLogo}`} alt="MakerDAO" className="title-cell" />
            {/* <h5 className="feature-title">MakerDAO</h5> */}
          </div>
          <div className="column-cell" style={{ marginTop: 30 }}>
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
        </div>
        <div className="why-mogul-column">
          <div className="column-cell">
            <img
              src={`${instreamlyLogo}`}
              alt="InSTREAMLY"
              className="title-cell"
            />
            {/* <h5 className="feature-title">InSTREAMLY</h5> */}
          </div>
          <div className="column-cell" style={{ marginTop: 30 }}>
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
          <div className="column-cell">
            <LikeMark positive />
          </div>
          <div className="column-cell">
            <LikeMark negative />
          </div>
        </div>
      </div>

      <hr
        style={{
          border: '1px solid rgb(246, 245, 247)',
          width: '100%',
          borderBottom: 'none',
          margin: 0,
        }}
      />

      <div id="mgul-undone">
        <div
          className="gradient-container"
          style={{
            background:
              'linear-gradient(180deg, #341D55 0%, rgba(44, 21, 77, 0.7) 100%)',
          }}
        >
          <p
            className="intro-title"
            style={{
              fontSize: 24,
              position: 'absolute',
              top: -30,
              margin: 0,
            }}
          >
            token information
          </p>
          <p className="white-text" style={{ margin: 0 }}>
            <b>MOGUL implements cutting-edge technology</b>
            <br />
            The MOGUL platform will consist of the following layers:
            <ul style={{ marginTop: 0 }}>
              <li>
                <b>Web app</b> in the form of a PWA React.js application,
              </li>
              <li>
                <b>Mobile app</b> in the form of an Android and iOS React Native
                app,
              </li>
              <li>
                The <b>blockchain engine</b> where the magic happens – manages
                the flow of the tokens are all located, in the form of Ethereum
                protocols and Solidity Smart Contracts,
              </li>
              <li>
                An Ethereum node which will read and sign the blockchain
                transactions,
              </li>
              <li>
                The API interconnecting all parts, in the form of a Node.js
                Loopback API.
              </li>
            </ul>
            MOGUL Coin is of ERC-20 standard and is used for all transactions on
            the platform.
            <br />
            <br />
            The MOGUL NFT token implements ERC-721 standard on Ethereum protocol
            where each copy is uniquely defined by a streamer, and a
            corresponding value in MGUL is assigned to each MOGUL NFT by the
            streamer.
          </p>
        </div>
      </div>

      <img src={gameBoy} id="gameboy" alt="Game Boy" width={794} />
      <div className="fake-foot" style={{ backgroundColor: '#54F7C5' }} />
    </div>

    <div className="join-mogul-alternative">
      <div className="join-mogul-alternative-content">
        <h2 className="join-mogul-alternative-title">
          Become a MOGUL – join the MoGUL token sale!
        </h2>
        <div className="join-mogul-alternative-text-container">
          <p
            className="join-mogul-alternative-text"
            style={{ marginRight: 80 }}
          >
            The MOGUL Coin (MGUL) token sale is LIVE now. If you want to be a
            part of this revolutionary project, you can now purchase MOGUL
            Coins.
          </p>
          <p className="join-mogul-alternative-text">
            Upon purchasing your MOGUL Coins, they will immediately be deposited
            in your MOGUL account, and you can track the value of your wallet in
            real time. After the ICO officially ends, you will then be able to
            withdraw and trade your MOGUL Coins.
          </p>
        </div>
      </div>
    </div>

    <div className="make-friends">
      <RecordsPanel />

      <div className="make-friends-content">
        <div className="any-questions">
          <div>
            <h2 className="intro-title" style={{ margin: 0 }}>
              Got{' '}
              <span style={{ color: '#D10286' }} className="blink">
                questions
              </span>{' '}
              about the ICO?
            </h2>
            <p
              style={{
                fontFamily: 'cubano',
                fontWeight: 400,
                fontSize: 24,
                color: '#FFF',
                opacity: 0.2,
                margin: '20px 0 0 0',
              }}
            >
              we’ll aim to get back to you within 24 hours.
            </p>
          </div>
          <h3
            className="intro-title"
            style={{ fontSize: 36, color: '#D10286', margin: 0 }}
            id="contact-email"
          >
            contact@mogulcoin.io
          </h3>
        </div>

        <hr
          style={{
            opacity: '0.06',
            margin: '80px 0',
            position: 'absolute',
            width: '100%',
            left: 0,
          }}
        />

        <div className="make-friends-explanation">
          <div className="make-friends-explanation-content">
            <h2 className="intro-title" style={{ margin: 0, width: '70%' }}>
              Make your friend a{' '}
              <span
                style={{ color: '#D10286', textDecoration: 'underline' }}
                className="blink"
              >
                MOGUL
              </span>{' '}
              and{' '}
              <span
                style={{ color: '#D10286', textDecoration: 'underline' }}
                className="blink"
              >
                EARN
              </span>{' '}
              Ethereum
            </h2>

            <img
              src={consoles}
              alt="Consoles"
              style={{
                position: 'absolute',
                left: 90,
                bottom: -165,
                zIndex: 20,
              }}
              id="consoles"
            />
          </div>
          <div className="make-friends-explanation-content">
            <p className="white-text" style={{ margin: '0 0 40px 0' }}>
              By participating in our special Referral Program, you can
              recommend MOGUL Coin to your friends, and earn ETH in the process.
            </p>
            <p className="white-text" style={{ margin: '0 0 40px 0' }}>
              It’s very easy. All you need to do is generate your personal
              referral link and send it to your friends or post it on your
              social media. After, you can kick back and relax, because for
              everyone who invests using your referral link, you can make:
            </p>
            <p className="lime-text" style={{ margin: '0 0 20px 0' }}>
              20% of the Ethereum that your friends contribute, or;
            </p>
            <p className="lime-text" style={{ margin: '0 0 80px 0' }}>
              30% of the MOGUL Coin that your friends purchase.
            </p>
            <div className="gradient-container" style={{ margin: 0 }}>
              <p
                className="intro-title"
                style={{
                  fontSize: 24,
                  position: 'absolute',
                  top: -30,
                  margin: 0,
                }}
              >
                FOR EXAMPLE
              </p>
              <p className="pink-text" style={{ marginTop: 0 }}>
                You sent your referral link to someone who purchased 1,000,000
                MOGUL Coins for 10 ETH.
              </p>
              <p className="pink-text" style={{ marginBottom: 0 }}>
                You will now automatically receive either 300,000 MOGUL Coins,
                or 2 ETH, depending on which option you choose.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <RoadMap />

    <div className="mogul-team">
      <div className="mogul-team-content">
        <h2
          className="intro-title"
          style={{
            margin: 0,
            width: 'fit-content',
            alignSelf: 'center',
            textAlign: 'center',
          }}
        >
          MEET OUR DEDICATED{' '}
          <span
            style={{ color: '#D10286', textDecoration: 'underline' }}
            className="blink"
          >
            MOGUL
          </span>{' '}
          TEAM
        </h2>

        <div className="mogul-team-members">
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name">Milosz Mach</p> */}
          {/*  <p className="member-role"> */}
          {/*    Brand Ambassador Officer rejon: Polska */}
          {/*  </p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name">Jakub Lukaszewski</p> */}
          {/*  <p className="member-role"> */}
          {/*    Brand Ambassador Officer rejon: Polska */}
          {/*  </p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name">Marwan Ibrahim</p> */}
          {/*  <p className="member-role"> */}
          {/*    Brand Ambassador Officer rejon: North Africa */}
          {/*  </p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name">Kazi Shafin</p> */}
          {/*  <p className="member-role"> */}
          {/*    Brand Ambassador Officer rejon: South Asia */}
          {/*  </p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name">Weidun Chang</p> */}
          {/*  <p className="member-role"> */}
          {/*    Brand Ambassador Officer rejon: South East Asia */}
          {/*  </p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name one-row">Dymitr</p> */}
          {/*  <p className="member-role">Chief Marketing Officer</p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  <img src={filipMartyni} alt="Member" className="member-photo" /> */}
          {/*  <p className="intro-title member-name">Filip Martyni-Orenowicz</p> */}
          {/*  <p className="member-role">COO</p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name">Patryk D</p> */}
          {/*  <p className="member-role"> */}
          {/*    Brand Ambassador Officer rejon: South America */}
          {/*  </p> */}
          {/* </div> */}
          {/* <div className="member"> */}
          {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
          {/*  <div className="fake-photo" /> */}
          {/*  <p className="intro-title member-name">Patryk Mankowski</p> */}
          {/*  <p className="member-role"> */}
          {/*    Brand Ambassador Officer rejon: South America */}
          {/*  </p> */}
          {/* </div> */}
          <div className="member">
            <img src={philippSiemek} alt="Member" className="member-photo" />
            <p className="intro-title member-name">Philipp Siemek</p>
            <p
              className="member-role"
              style={{ display: 'flex', alignItems: 'center' }}
            >
              FOUNDER & CEO
              <a
                href="https://www.linkedin.com/in/philippsiemek/"
                className="contact-link"
                style={{ marginRight: 13 }}
              >
                <img
                  src={linkedinLogo}
                  alt="LinkedIn"
                  style={{ width: 35, marginLeft: 15 }}
                />
              </a>
            </p>
          </div>
          <div className="member">
            <img src={filipMartyni} alt="Member" className="member-photo" />
            <p className="intro-title member-name">Filip Martyni-Orenowicz</p>
            <p
              className="member-role"
              style={{ display: 'flex', alignItems: 'center' }}
            >
              COO
              <a
                href="https://www.linkedin.com/in/filip-martyni-orenowicz-7567b9181/"
                className="contact-link"
                style={{ marginRight: 13 }}
              >
                <img
                  src={linkedinLogo}
                  alt="LinkedIn"
                  style={{ width: 35, marginLeft: 15 }}
                />
              </a>
            </p>
          </div>
          <div className="member">
            <img src={dymitrLubczyk} alt="Member" className="member-photo" />
            <p className="intro-title member-name">Dymitr Lubczyk</p>
            <p
              className="member-role"
              style={{ display: 'flex', alignItems: 'center' }}
            >
              CMO
              <a
                href="https://www.linkedin.com/in/dymitr-lubczyk-867748209/"
                className="contact-link"
                style={{ marginRight: 13 }}
              >
                <img
                  src={linkedinLogo}
                  alt="LinkedIn"
                  style={{ width: 35, marginLeft: 15 }}
                />
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>

    <Footer />
  </div>
)

export default Body
