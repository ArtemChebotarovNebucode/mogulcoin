import React from 'react'

import '../MainMarketing.css'
import Header from './Header/Header'
import Body from './Body'

const MainExtended = () => (
  <div className="screen">
    <Header />
    <Body />
  </div>
)

export default MainExtended
