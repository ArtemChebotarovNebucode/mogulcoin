import React from 'react'
import '../../MainMarketing.css'

import background from '../../../../assets/raster/header-background-test.png'
import limeLine from '../../../../assets/raster/lime-line.png'
import starIcon from '../../../../assets/vector/star-icon.svg'
import streamIcon from '../../../../assets/vector/stream-icon.svg'
import fanIcon from '../../../../assets/vector/fan-icon.svg'
import whitePaper from '../../../../assets/vector/whitepaper-icon.svg'
import sendIcon from '../../../../assets/vector/send-lime-icon.svg'
import floorTint from '../../../../assets/raster/floor-tint.png'

import { NavBar } from '../../../../components'
import LaunchpadPanel from './LaunchpadPanel/LaunchpadPanel'
import TokenPricingPanel from './TokenPricingPanel/TokenPricingPanel'

const Header = () => (
  <div className="header">
    <div className="intro">
      <div
        style={{
          backgroundImage: `url(${background})`,
        }}
        className="header-visual"
      >
        <div className="animated-floor" />
        <img
          src={floorTint}
          alt="Tint"
          style={{
            position: 'absolute',
            bottom: 0,
            height: 330,
            width: '100%',
          }}
        />
        <img
          src={limeLine}
          alt="Line Decorative"
          style={{ position: 'absolute', bottom: '30%', zIndex: 2 }}
        />
      </div>

      <NavBar isExtended />

      <div className="title-section" style={{ zIndex: 2 }}>
        <h1
          className="title-huge"
          style={{
            marginTop: 80,
            marginBottom: 32,
            width: '70%',
            alignSelf: 'center',
          }}
          id="huge-main-extended"
        >
          MAJOR ICO EVENT of 2021
        </h1>
        <h3
          className="subtitle-lime"
          style={{
            margin: 0,
            marginBottom: 32,
            width: '70%',
            alignSelf: 'center',
          }}
        >
          Welcome to the hottest hub for the streaming community
        </h3>
        <h4
          className="subtitle-white"
          style={{
            margin: 0,
            marginBottom: 72,
            width: '70%',
            alignSelf: 'center',
          }}
        >
          introducing the MOGUL NFT Marketplace
        </h4>

        <div className="sign-up-section">
          <a
            href="/#"
            style={{ marginRight: 49 }}
            id="download-whitepaper"
            className="header-link"
          >
            DOWNLOAD WHITEPAPER
            <img src={whitePaper} alt="Whitepaper" style={{ marginLeft: 10 }} />
          </a>

          <a href="/#" id="join-crowsdale" className="header-link">
            JOIN CROWDSALE
            <img src={sendIcon} alt="Crowdsale" style={{ marginLeft: 10 }} />
          </a>
        </div>

        <div className="info-panels">
          <LaunchpadPanel />
          <TokenPricingPanel />
        </div>

        <h2
          className="title-big"
          id="big-main"
          style={{ margin: '120px 0', width: '60%', alignSelf: 'center' }}
        >
          JOIN OUR DYNAMICALLY GROWING ECOSYSTEM OF{' '}
          <span className="pink-insertion blink">BRANDS</span>,{' '}
          <span className="pink-insertion blink">STREAMERS</span> AND{' '}
          <span className="pink-insertion blink">FANS</span>
        </h2>
      </div>
    </div>

    <div className="outro">
      <div className="section">
        <div className="section-header">
          <img src={starIcon} alt="Brands" />
          <h3 className="section-title">BRANDS</h3>
        </div>
        <p className="white-text" style={{ margin: 0 }}>
          As a brand, you will gain access to countless streamers who are
          willing to advertise your products on their channels.{' '}
        </p>
        <p className="lime-text" style={{ margin: '35px 0' }}>
          Gain the exposure you need to grow your brand, and watch your returns
          explode.
        </p>
        <p className="white-text" style={{ margin: 0 }}>
          All you need to do is sign up, and our advanced Smart Contracts will
          allow you to advertise your products on thousands of channels
          simultaneously!
        </p>
      </div>
      <div className="section">
        <div className="section-header">
          <img src={`${streamIcon}`} alt="Brands" />
          <p className="section-title">STREAMERS</p>
        </div>
        <p className="white-text" style={{ margin: 0 }}>
          MOGUL is the ideal tool for you to increase your recognition in the
          market, expand your channel to new heights, and turn your passion into
          your lifestyle.
        </p>
        <p className="lime-text" style={{ margin: '40px 0 28px 0' }}>
          Connect with countless brands
        </p>
        <p className="lime-text" style={{ marginBottom: 28 }}>
          Create your personal token
        </p>
        <p className="lime-text" style={{ margin: 0 }}>
          Receive subscriptions and donations from your fans
        </p>
      </div>
      <div className="section">
        <div className="section-header">
          <img src={`${fanIcon}`} alt="Brands" />
          <p className="section-title">FANS</p>
        </div>
        <p className="white-text" style={{ margin: 0, marginBottom: 30 }}>
          At MOGUL, you will not only be able to enjoy your favorite content
          from the streamers you follow, but also support them by subscribing
          for premium content and donating to their channel using custom tokens.
        </p>
        <p className="lime-text" style={{ margin: 0 }}>
          Your support will mean that the streamers can drive up the frequency
          with which they stream and increase the quality of the content in many
          ways.
        </p>
      </div>
    </div>

    <div style={{ position: 'relative' }}>
      <h1
        className="text-separator left central-side"
        style={{ marginTop: 120, marginBottom: 60 }}
      >
        UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING THE
        POWER OF NFT -{' '}
      </h1>

      <h1
        className="text-separator left left-side"
        style={{
          position: 'absolute',
          top: 0,
          left: -2790,
          marginTop: 120,
        }}
      >
        UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING THE
        POWER OF NFT -{' '}
      </h1>

      <h1
        className="text-separator left right-side"
        style={{
          position: 'absolute',
          top: 0,
          left: 2790,
          marginTop: 120,
        }}
      >
        UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING THE
        POWER OF NFT -{' '}
      </h1>
    </div>

    <div style={{ position: 'relative' }}>
      <h1
        className="text-separator right central-side"
        style={{ marginBottom: 120 }}
      >
        UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING THE
        POWER OF NFT -{' '}
      </h1>

      <h1
        className="text-separator right left-side"
        style={{
          position: 'absolute',
          top: 0,
          left: -2790,
        }}
      >
        UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING THE
        POWER OF NFT -{' '}
      </h1>

      <h1
        className="text-separator right right-side"
        style={{
          position: 'absolute',
          top: 0,
          left: 2790,
        }}
      >
        UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING THE
        POWER OF NFT -{' '}
      </h1>
    </div>
  </div>
)

export default Header
