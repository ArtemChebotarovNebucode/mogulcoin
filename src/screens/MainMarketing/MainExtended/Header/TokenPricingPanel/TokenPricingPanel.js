import React from 'react'

import './TokenPricingPanel.css'
import rightArrow from '../../../../../assets/vector/right-pink-arrow.svg'
import downArrow from '../../../../../assets/vector/down-arrow.svg'
import { useMain } from '../../../../../contexts/MainProvider/MainProvider'

const TokenPricingPanel = () => {
  const { windowWidth } = useMain()

  return (
    <div className="token-pricing-panel">
      <p className="token-pricing-title">
        PHASE 1{' '}
        <span style={{ color: '#D10286' }} className="blink">
          TOKEN PRICING
        </span>
      </p>

      <div className="token-pricing-content">
        <div className="token-pricing-usd">
          <p
            style={{
              fontFamily: 'cubano',
              fontWeight: 400,
              fontSize: 16,
              color: '#D10286',
              margin: 0,
              marginBottom: 6,
            }}
          >
            Buy
          </p>
          <p
            style={{
              fontFamily: 'cubano',
              fontWeight: 400,
              fontSize: 32,
              color: 'white',
              margin: 0,
              display: 'flex',
              alignItems: 'flex-end',
              flexWrap: 'nowrap',
            }}
          >
            50,00{' '}
            <span
              style={{
                fontFamily: 'cubano',
                fontWeight: 400,
                fontSize: 16,
                color: '#D10286',
                marginLeft: 10,
              }}
            >
              USD
            </span>
          </p>
        </div>

        <img
          src={windowWidth > 1616 ? rightArrow : downArrow}
          alt="Arrow"
          style={
            windowWidth > 1616
              ? { margin: '0 60px', paddingTop: 25 }
              : { margin: '24px 0' }
          }
        />

        <div className="token-pricing-mgl">
          <p
            style={{
              fontFamily: 'cubano',
              fontWeight: 400,
              fontSize: 16,
              color: '#D10286',
              margin: 0,
              marginBottom: 6,
            }}
          >
            Buy
          </p>
          <p
            style={{
              fontFamily: 'cubano',
              fontWeight: 400,
              fontSize: 32,
              color: 'white',
              margin: 0,
              whiteSpace: 'nowrap',
            }}
          >
            1700,00{' '}
            <span
              style={{
                fontFamily: 'cubano',
                fontWeight: 400,
                fontSize: 16,
                color: '#D10286',
              }}
            >
              MGL
            </span>
          </p>
        </div>
      </div>
      <div id="floating-sign">
        <div style={{ position: 'relative', height: 12 }}>
          <p
            style={{
              fontFamily: 'Montserrat',
              color: '#D10286',
              fontWeight: 400,
              fontSize: 12,
              whiteSpace: 'nowrap',
              zIndex: 4,
              // position: 'absolute',
              top: 0,
              margin: 0,
            }}
          >
            INCLUDING THE EARLY BIRD BONUS - INCLUDING THE EARLY BIRD BONUS -
            INCLUDING THE EARLY BIRD BONUS -{' '}
          </p>

          <p
            style={{
              fontFamily: 'Montserrat',
              color: '#D10286',
              fontWeight: 400,
              fontSize: 12,
              whiteSpace: 'nowrap',
              zIndex: 4,
              position: 'absolute',
              top: 0,
              margin: 0,
              left: -710,
            }}
          >
            INCLUDING THE EARLY BIRD BONUS - INCLUDING THE EARLY BIRD BONUS -
            INCLUDING THE EARLY BIRD BONUS -{' '}
          </p>

          <p
            style={{
              fontFamily: 'Montserrat',
              color: '#D10286',
              fontWeight: 400,
              fontSize: 12,
              whiteSpace: 'nowrap',
              zIndex: 4,
              position: 'absolute',
              top: 0,
              margin: 0,
              left: 711,
            }}
          >
            INCLUDING THE EARLY BIRD BONUS - INCLUDING THE EARLY BIRD BONUS -
            INCLUDING THE EARLY BIRD BONUS -{' '}
          </p>
        </div>
      </div>
    </div>
  )
}

export default TokenPricingPanel

// <div style={{ position: 'relative' }}>
// <h1
// className="text-separator left"
// style={{ marginTop: 120, marginBottom: 60 }}
// >
// UTILIZING THE POWER OF NFT&apos;S - UTILIZING THE POWER OF NFT&apos;S
// - UTILIZING THE POWER OF NFT&apos;S -{' '}
// </h1>
//
// <h1
// className="text-separator left"
// style={{
// position: 'absolute',
// top: 0,
// left: '-155%',
// marginTop: 120,
// }}
// >
// UTILIZING THE POWER OF NFT&apos;S - UTILIZING THE POWER OF NFT&apos;S
// - UTILIZING THE POWER OF NFT&apos;S -{' '}
// </h1>
//
// <h1
// className="text-separator left"
// style={{
// position: 'absolute',
// top: 0,
// left: '155%',
// marginTop: 120,
// }}
// >
// UTILIZING THE POWER OF NFT&apos;S - UTILIZING THE POWER OF NFT&apos;S
// - UTILIZING THE POWER OF NFT&apos;S -{' '}
// </h1>
// </div>
