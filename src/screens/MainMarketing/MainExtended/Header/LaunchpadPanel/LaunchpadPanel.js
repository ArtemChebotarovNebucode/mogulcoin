import React from 'react'

import './LaunchpadPanel.css'
import { StatusBar } from '../../../../../components'

const LaunchpadPanel = () => (
  <div className="launchpad-panel">
    <h3 className="launchpad-panel-title">
      LAUNCHPAD{' '}
      <span style={{ color: '#54F7C5' }} className="blink">
        PHASE 1
      </span>{' '}
      IS LIVE
    </h3>

    <p
      className="gray-text"
      style={{
        fontSize: 14,
        alignSelf: 'flex-end',
        margin: 0,
        width: 135,
        textAlign: 'right',
        marginBottom: 34,
        lineHeight: '24px',
        fontWeight: 'normal',
      }}
    >
      Price of{' '}
      <span style={{ fontWeight: 600, color: '#54F7C5' }}>0.0113 USD</span> per{' '}
      <span style={{ fontWeight: 600, color: '#54F7C5' }}>1 MGL</span>
    </p>

    <StatusBar percent={70} from="0" to="100K USD" />

    <div className="current-status">
      <p
        className="current-status-title"
        style={{ color: '#54F7C5', marginRight: 30 }}
      >
        69,722 <span id="usd-text">USd</span>
      </p>
      <hr className="status-separator" />
      <p
        className="current-status-title"
        style={{ color: '#54F7C5', marginLeft: 30 }}
        id="raised-text"
      >
        RAISED
      </p>
    </div>
  </div>
)

export default LaunchpadPanel
