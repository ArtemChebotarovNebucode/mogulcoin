import React, { useState } from 'react'

import '../../MainMarketing.css'

import slotMachine from '../../../../assets/raster/slot-machine.png'
import sun from '../../../../assets/raster/sun.png'
import gameBoy from '../../../../assets/raster/gameboy.png'
import dymitrLubczyk from '../../../../assets/raster/team-members/dymitr-lubczyk.png'
import filipMartyni from '../../../../assets/raster/team-members/filip-martyni.jpg'
import { Footer, RoadMap, TextInput, LikeMark } from '../../../../components'
import backgroundCircles from '../../../../assets/vector/background-circles.svg'
import philippSiemek from '../../../../assets/raster/team-members/ceo-photo.jpeg'
import linkedinLogo from '../../../../assets/vector/linkedin-logo.svg'
import logoWrapped from '../../../../assets/vector/logo-wrapped.svg'
import animeCoinLogo from '../../../../assets/raster/animecoin.jpg'
import rallyLogo from '../../../../assets/raster/rally.jpg'
import refereumLogo from '../../../../assets/raster/refereum.jpg'
import indahashLogo from '../../../../assets/raster/indahash.jpg'
import makerLogo from '../../../../assets/raster/maker.jpg'
import instreamlyLogo from '../../../../assets/raster/instreamly.jpg'

// eslint-disable-next-line react/prop-types
const Body = ({ nftRef, whyMogulRef, roadMapRef, teamRef }) => {
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState(null)

  const handleOnChange = (e) => {
    const { value } = e.target
    setEmail(value)
  }

  const isEmail = (_email) => {
    // eslint-disable-next-line no-useless-escape
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    return re.test(String(_email).toLowerCase())
  }

  const handleOnSubmit = (e) => {
    e.preventDefault()

    if (!isEmail(email)) {
      setMessage('Invalid email address')
      return
    }

    fetch('https://api.mogulcoin.io/newsletter', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({ email }),
    })
      .then((res) => res.json())
      .then((res) => {
        setMessage(
          res.error
            ? res.error.message
            : 'You have been successfully signed up - check your email.',
        )
      })
  }

  return (
    <div className="intro">
      <div className="nft-intro" ref={nftRef}>
        <h2 className="intro-title" style={{ margin: 0 }}>
          INTRODUCING THE MOGUL NFT MARKETPLACE
        </h2>

        <div className="nft-explained">
          <div className="text-section">
            <p className="white-text">
              Become a MOGUL in <span style={{ color: '#D10286' }}>every</span>{' '}
              sense of the word with our brand new NFT Marketplace!
            </p>
            <p className="white-text">
              Join exclusive streaming communities and participate in one of the
              first NFT Marketplaces for streamers in the world.
            </p>

            <div className="gradient-container">
              <p
                className="intro-title"
                style={{
                  fontSize: 24,
                  position: 'absolute',
                  top: -30,
                  margin: 0,
                }}
              >
                STREAMERS
              </p>
              <p className="pink-text" style={{ margin: 0 }}>
                can now generate a finite number of collectibles, in the form of
                either electronic artwork or in the form of special content
                videos, to be either sold or auctioned for MGUL – allowing
                streamers to crowdfund their newest projects.
              </p>
            </div>

            <p className="white-text">
              Anyone can place a bid for this exclusive content released by
              streamers – creating a unique addition to your collection of
              electronic artwork.
            </p>
            <p className="white-text" id="nft-text-bottom">
              On the MOGUL NFT Marketplace, you can not only find hidden gems,
              but also exchange them for other NFTs with your fellow streaming
              fans.
            </p>
            <p
              className="white-text"
              id="nft-text-bottom"
              style={{ marginBottom: 120 }}
            >
              <span style={{ color: '#D10286' }}>
                Become a true MOGUL by becoming a master at locating the best
                NFTs on the market
              </span>{' '}
              or simply enjoy your ever-growing art collection.
            </p>
          </div>

          <div className="pics-section">
            <img
              src={backgroundCircles}
              alt="Circles"
              style={{
                position: 'absolute',
                bottom: 0,
                right: 0,
              }}
              id="circles"
            />
            <img
              src={slotMachine}
              alt="Slot Machine"
              id="slot-machine"
              style={{
                zIndex: 1,
                position: 'absolute',
                bottom: 0,
                right: -100,
              }}
            />
          </div>
        </div>
      </div>

      <div className="become-mogul">
        <img src={sun} alt="Sun" id="sun" />

        <div className="become-mogul-content">
          <h2 className="intro-title" id="streaming-title">
            STREAMING IS ONE OF THE FASTEST GROWING AND MOST POPULAR TOOLS FOR
            CONTENT CREATORS WORLDWIDE!
          </h2>
        </div>
        <div className="become-mogul-content" id="become-mogul-content-text">
          <p className="pink-text" id="streaming-text">
            Don’t sleep on streaming – join an exploding{' '}
            <span style={{ fontWeight: 700, color: 'white' }}>
              $50 billion market
            </span>{' '}
            and become a MOGUL
          </p>
        </div>
      </div>

      <div className="why-mogul" ref={whyMogulRef}>
        <div className="why-mogul-title-rwd">
          <h2 className="why-mogul-title rwd">
            WHY{' '}
            <span
              style={{
                color: '#D10286',
                textDecoration: 'underline',
                margin: 0,
              }}
              className="blink"
            >
              MOGUL
            </span>
            ?
          </h2>
        </div>
        <div className="why-mogul-content">
          <div
            className="why-mogul-column title-column"
            style={{ flexGrow: 0.8, padding: '89px 20px 90px 0' }}
          >
            <div
              className="column-cell"
              style={{ justifyContent: 'flex-start' }}
            >
              <h2 className="why-mogul-title">
                WHY{' '}
                <span
                  style={{
                    color: '#D10286',
                    textDecoration: 'underline',
                    margin: 0,
                  }}
                  className="blink"
                >
                  MOGUL
                </span>
                ?
              </h2>
            </div>
            <div
              className="column-cell"
              style={{ marginTop: 30, justifyContent: 'flex-start' }}
            >
              <h5 className="feature-title">Blockchain technology</h5>
            </div>
            <div
              className="column-cell"
              style={{ justifyContent: 'flex-start' }}
            >
              <h5 className="feature-title">Connecting streamers and brands</h5>
            </div>
            <div
              className="column-cell"
              style={{ justifyContent: 'flex-start' }}
            >
              <h5 className="feature-title">NFT Marketplace</h5>
            </div>
            <div
              className="column-cell"
              style={{ justifyContent: 'flex-start' }}
            >
              <h5 className="feature-title">Facilitating ad placement</h5>
            </div>
            <div
              className="column-cell"
              style={{ justifyContent: 'flex-start' }}
            >
              <h5 className="feature-title">Customizable tokens</h5>
            </div>
          </div>
          <div className="why-mogul-column chosen">
            <div className="column-cell">
              <img
                src={`${logoWrapped}`}
                alt="Logo"
                className="title-cell"
                width={102}
                height={52}
              />
              {/* <h5 className="feature-title">MOGUL Coin</h5> */}
            </div>
            <div className="column-cell" style={{ marginTop: 30 }}>
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
          </div>
          <div
            className="why-mogul-column"
            style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
          >
            <div className="column-cell">
              <img
                src={`${animeCoinLogo}`}
                alt="Anime Coin"
                className="title-cell"
              />
              {/* <h5 className="feature-title">Anime Coin</h5> */}
            </div>
            <div className="column-cell" style={{ marginTop: 30 }}>
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
          </div>
          <div
            className="why-mogul-column"
            style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
          >
            <div className="column-cell">
              <img src={`${rallyLogo}`} alt="Rally" className="title-cell" />
              {/* <h5 className="feature-title">Rally</h5> */}
            </div>
            <div className="column-cell" style={{ marginTop: 30 }}>
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
          </div>
          <div
            className="why-mogul-column"
            style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
          >
            <div className="column-cell">
              <img
                src={`${refereumLogo}`}
                alt="Refereum"
                className="title-cell"
              />
              {/* <h5 className="feature-title">Refereum</h5> */}
            </div>
            <div className="column-cell" style={{ marginTop: 30 }}>
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
          </div>
          <div
            className="why-mogul-column"
            style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
          >
            <div className="column-cell">
              <img
                src={`${indahashLogo}`}
                alt="Inda Hash"
                className="title-cell"
              />
              {/* <h5 className="feature-title">indaHash</h5> */}
            </div>
            <div className="column-cell" style={{ marginTop: 30 }}>
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
          </div>
          <div
            className="why-mogul-column"
            style={{ borderRight: '1px solid rgb(246, 245, 247)' }}
          >
            <div className="column-cell">
              <img src={`${makerLogo}`} alt="MakerDAO" className="title-cell" />
              {/* <h5 className="feature-title">MakerDAO</h5> */}
            </div>
            <div className="column-cell" style={{ marginTop: 30 }}>
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
          </div>
          <div className="why-mogul-column">
            <div className="column-cell">
              <img
                src={`${instreamlyLogo}`}
                alt="InSTREAMLY"
                className="title-cell"
              />
              {/* <h5 className="feature-title">InSTREAMLY</h5> */}
            </div>
            <div className="column-cell" style={{ marginTop: 30 }}>
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
            <div className="column-cell">
              <LikeMark positive />
            </div>
            <div className="column-cell">
              <LikeMark negative />
            </div>
          </div>
        </div>

        <hr
          style={{
            border: '1px solid rgb(246, 245, 247)',
            width: '100%',
            borderBottom: 'none',
            margin: 0,
          }}
        />

        <div id="mgul-undone">
          <div
            className="gradient-container"
            style={{
              background:
                'linear-gradient(180deg, #341D55 0%, rgba(44, 21, 77, 0.7) 100%)',
            }}
          >
            <p
              className="intro-title"
              style={{
                fontSize: 24,
                position: 'absolute',
                top: -30,
                margin: 0,
              }}
            >
              token information
            </p>
            <p className="white-text" style={{ margin: 0 }}>
              <b>MOGUL implements cutting-edge technology</b>
              <br />
              The MOGUL platform will consist of the following layers:
              <ul style={{ marginTop: 0 }}>
                <li>
                  <b>Web app</b> in the form of a PWA React.js application,
                </li>
                <li>
                  <b>Mobile app</b> in the form of an Android and iOS React
                  Native app,
                </li>
                <li>
                  The <b>blockchain engine</b> where the magic happens – manages
                  the flow of the tokens are all located, in the form of
                  Ethereum protocols and Solidity Smart Contracts,
                </li>
                <li>
                  An Ethereum node which will read and sign the blockchain
                  transactions,
                </li>
                <li>
                  The API interconnecting all parts, in the form of a Node.js
                  Loopback API.
                </li>
              </ul>
              MOGUL Coin is of ERC-20 standard and is used for all transactions
              on the platform.
              <br />
              <br />
              The MOGUL NFT token implements ERC-721 standard on Ethereum
              protocol where each copy is uniquely defined by a streamer, and a
              corresponding value in MGUL is assigned to each MOGUL NFT by the
              streamer.
            </p>
          </div>
        </div>

        <img src={gameBoy} id="gameboy" alt="Game Boy" width={794} />
        <div className="fake-foot" />
      </div>

      <div className="join-mogul">
        <div className="join-mogul-content">
          <div className="join-mogul-content-left">
            <p className="join-mogul-title">JOIN THE EXCLUSIVE MOGUL AIRDROP</p>
            <p className="join-mogul-text" style={{ margin: '32px 0 0 0' }}>
              Whether you are a streamer or a fan, you can now sign up to our
              mailing list and receive up to 40,000 MOGUL Coins for{' '}
              <span style={{ color: '#54F7C5' }}>free</span>
            </p>
          </div>
          <div className="join-mogul-content-right">
            <form onSubmit={handleOnSubmit}>
              <TextInput
                placeholder="ENTER E-MAIL ADDRESS"
                onChange={handleOnChange}
                value={email}
                styleId={1}
              />
              {message && (
                <p style={{ marginTop: 10 }} className="white-text">
                  {message}
                </p>
              )}
            </form>
          </div>
        </div>
      </div>

      <RoadMap roadMapRef={roadMapRef} />

      <div className="mogul-team" ref={teamRef}>
        <div className="mogul-team-content">
          <h2
            className="intro-title"
            style={{
              margin: 0,
              width: 'fit-content',
              alignSelf: 'center',
              textAlign: 'center',
            }}
          >
            MEET OUR DEDICATED{' '}
            <span
              style={{ color: '#D10286', textDecoration: 'underline' }}
              className="blink"
            >
              MOGUL
            </span>{' '}
            TEAM
          </h2>

          <div className="mogul-team-members">
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name">Milosz Mach</p> */}
            {/*  <p className="member-role"> */}
            {/*    Brand Ambassador Officer rejon: Polska */}
            {/*  </p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name">Jakub Lukaszewski</p> */}
            {/*  <p className="member-role"> */}
            {/*    Brand Ambassador Officer rejon: Polska */}
            {/*  </p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name">Marwan Ibrahim</p> */}
            {/*  <p className="member-role"> */}
            {/*    Brand Ambassador Officer rejon: North Africa */}
            {/*  </p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name">Kazi Shafin</p> */}
            {/*  <p className="member-role"> */}
            {/*    Brand Ambassador Officer rejon: South Asia */}
            {/*  </p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name">Weidun Chang</p> */}
            {/*  <p className="member-role"> */}
            {/*    Brand Ambassador Officer rejon: South East Asia */}
            {/*  </p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name one-row">Dymitr</p> */}
            {/*  <p className="member-role">Chief Marketing Officer</p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  <img src={filipMartyni} alt="Member" className="member-photo" /> */}
            {/*  <p className="intro-title member-name">Filip Martyni-Orenowicz</p> */}
            {/*  <p className="member-role">COO</p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name">Patryk D</p> */}
            {/*  <p className="member-role"> */}
            {/*    Brand Ambassador Officer rejon: South America */}
            {/*  </p> */}
            {/* </div> */}
            {/* <div className="member"> */}
            {/*  /!* <img src={ceoPhoto} alt="Member" className="member-photo" /> *!/ */}
            {/*  <div className="fake-photo" /> */}
            {/*  <p className="intro-title member-name">Patryk Mankowski</p> */}
            {/*  <p className="member-role"> */}
            {/*    Brand Ambassador Officer rejon: South America */}
            {/*  </p> */}
            {/* </div> */}
            <div className="member">
              <img src={philippSiemek} alt="Member" className="member-photo" />
              <p className="intro-title member-name">Philipp Siemek</p>
              <p
                className="member-role"
                style={{ display: 'flex', alignItems: 'center' }}
              >
                FOUNDER & CEO
                <a
                  href="https://www.linkedin.com/in/philippsiemek/"
                  className="contact-link"
                  style={{ marginRight: 13 }}
                >
                  <img
                    src={linkedinLogo}
                    alt="LinkedIn"
                    style={{ width: 35, marginLeft: 15 }}
                    className="member-linkedin-link"
                  />
                </a>
              </p>
            </div>
            <div className="member">
              <img src={filipMartyni} alt="Member" className="member-photo" />
              <p className="intro-title member-name">Filip Martyni-Orenowicz</p>
              <p
                className="member-role"
                style={{ display: 'flex', alignItems: 'center' }}
              >
                COO
                <a
                  href="https://www.linkedin.com/in/filip-martyni-orenowicz-7567b9181/"
                  className="contact-link"
                  style={{ marginRight: 13 }}
                >
                  <img
                    src={linkedinLogo}
                    alt="LinkedIn"
                    style={{ width: 35, marginLeft: 15 }}
                    className="member-linkedin-link"
                  />
                </a>
              </p>
            </div>
            <div className="member">
              <img src={dymitrLubczyk} alt="Member" className="member-photo" />
              <p className="intro-title member-name">Dymitr Lubczyk</p>
              <p
                className="member-role"
                style={{ display: 'flex', alignItems: 'center' }}
              >
                CMO
                <a
                  href="https://www.linkedin.com/in/dymitr-lubczyk-867748209/"
                  className="contact-link"
                  style={{ marginRight: 13 }}
                >
                  <img
                    src={linkedinLogo}
                    alt="LinkedIn"
                    style={{ width: 35, marginLeft: 15 }}
                    className="member-linkedin-link"
                  />
                </a>
              </p>
            </div>
            {/* <div className="member"> */}
            {/*  <img src={weidunChang} alt="Member" className="member-photo" /> */}
            {/*  <p className="intro-title member-name">Weidun Chang</p> */}
            {/*  <p */}
            {/*    className="member-role" */}
            {/*    style={{ display: 'flex', alignItems: 'center' }} */}
            {/*  > */}
            {/*    Brand Ambassador Officer; SE Asia */}
            {/*    <a */}
            {/*      href="https://www.linkedin.com/in/weidun-chang-張惟敦-8233731a5/" */}
            {/*      className="contact-link" */}
            {/*      style={{ marginRight: 13 }} */}
            {/*    > */}
            {/*      <img */}
            {/*        src={linkedinLogo} */}
            {/*        alt="LinkedIn" */}
            {/*        style={{ width: 35, marginLeft: 15 }} */}
            {/*        className="member-linkedin-link" */}
            {/*      /> */}
            {/*    </a> */}
            {/*  </p> */}
            {/* </div> */}
          </div>
        </div>
      </div>

      <Footer />
    </div>
  )
}

export default Body
