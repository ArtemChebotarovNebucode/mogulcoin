import React, { useRef } from 'react'

import '../MainMarketing.css'
import Body from './Body'
import Header from './Header/Header'

const Main = () => {
  const nftRef = useRef(null)
  const whyMogulRef = useRef(null)
  const roadMapRef = useRef(null)
  const teamRef = useRef(null)

  return (
    <div className="screen">
      <Header
        nftRef={nftRef}
        whyMogulRef={whyMogulRef}
        roadMapRef={roadMapRef}
        teamRef={teamRef}
      />
      <Body
        nftRef={nftRef}
        whyMogulRef={whyMogulRef}
        roadMapRef={roadMapRef}
        teamRef={teamRef}
      />
    </div>
  )
}

export default Main
