import React, { useState } from 'react'
import '../../MainMarketing.css'

import background from '../../../../assets/raster/header-background-test.png'
import starIcon from '../../../../assets/vector/star-icon.svg'
import streamIcon from '../../../../assets/vector/stream-icon.svg'
import fanIcon from '../../../../assets/vector/fan-icon.svg'
import whitePaper from '../../../../assets/vector/whitepaper-icon.svg'

import { NavBar, TextInput } from '../../../../components'
import floorTint from '../../../../assets/raster/floor-tint.png'
import limeLine from '../../../../assets/raster/lime-line.png'

// eslint-disable-next-line react/prop-types
const Header = ({ nftRef, whyMogulRef, roadMapRef, teamRef }) => {
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState(null)

  const handleOnChange = (e) => {
    const { value } = e.target
    setEmail(value)
  }

  const isEmail = (_email) => {
    // eslint-disable-next-line no-useless-escape
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    return re.test(String(_email).toLowerCase())
  }

  const handleOnSubmit = (e) => {
    e.preventDefault()

    if (!isEmail(email)) {
      setMessage('Invalid email address')
      return
    }

    fetch('https://api.mogulcoin.io/newsletter', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({ email }),
    })
      .then((res) => res.json())
      .then((res) => {
        setMessage(
          res.error
            ? res.error.message
            : 'You have been successfully signed up - check your email.',
        )
      })
  }

  return (
    <div className="header">
      <div className="intro">
        <div
          style={{
            backgroundImage: `url(${background})`,
          }}
          className="header-visual"
        >
          <div className="animated-floor" />
          <img
            src={floorTint}
            alt="Tint"
            style={{
              position: 'absolute',
              bottom: 0,
              height: 330,
              width: '100%',
            }}
          />
          <img
            src={limeLine}
            alt="Line Decorative"
            style={{
              position: 'absolute',
              bottom: '30%',
              zIndex: 2,
              width: '100%',
            }}
          />
        </div>

        <NavBar
          nftRef={nftRef}
          whyMogulRef={whyMogulRef}
          roadMapRef={roadMapRef}
          teamRef={teamRef}
        />

        <div className="title-section" style={{ zIndex: 2 }}>
          <h1
            className="title-huge"
            style={{
              marginTop: 80,
              marginBottom: 32,
              width: '50%',
              alignSelf: 'center',
            }}
            id="huge-main"
          >
            JOIN THE EXCLUSIVE MOGUL AIRDROP
          </h1>
          <h3
            className="subtitle-lime"
            style={{
              margin: 0,
              marginBottom: 32,
              width: '70%',
              alignSelf: 'center',
            }}
          >
            FOR STREAMERS <span style={{ color: '#FFF' }}>AND</span> STREAMING
            FANS
          </h3>
          <h4
            className="subtitle-white"
            style={{
              margin: 0,
              marginBottom: 48,
              width: '70%',
              alignSelf: 'center',
            }}
          >
            YOU CAN NOW SIGN UP TO RECEIVE YOUR{' '}
            <span style={{ color: '#54F7C5' }}>FREE</span> MOGUL COINS
          </h4>

          <div className="sign-up-section">
            <form onSubmit={handleOnSubmit}>
              <TextInput
                placeholder="ENTER E-MAIL ADDRESS"
                onChange={handleOnChange}
                value={email}
                styleId={1}
              />
              {message && (
                <p style={{ marginTop: 10 }} className="white-text">
                  {message}
                </p>
              )}
            </form>

            <p className="gray-text" style={{ fontSize: 16, margin: '0 49px' }}>
              OR
            </p>

            <a
              id="whitepaper-link"
              href={`${process.env.PUBLIC_URL}/MOGUL_Whitepaper.pdf`}
              download
            >
              DOWNLOAD WHITEPAPER
              <img
                src={`${whitePaper}`}
                alt="Download Whitepaper"
                id="whitepaper-icon"
                style={{ marginLeft: 10 }}
              />
            </a>
          </div>

          <h2
            className="title-big"
            id="big-main"
            style={{
              margin: '128px 0 120px 0',
              width: '60%',
              alignSelf: 'center',
            }}
          >
            JOIN OUR DYNAMICALLY GROWING ECOSYSTEM OF{' '}
            <span className="pink-insertion blink">BRANDS</span>,{' '}
            <span className="pink-insertion blink">STREAMERS</span> AND{' '}
            <span className="pink-insertion blink">FANS</span>
          </h2>
        </div>
      </div>

      <div className="outro">
        <div className="section">
          <div className="section-header">
            <img src={starIcon} alt="Brands" />
            <h3 className="section-title">BRANDS</h3>
          </div>
          <p className="white-text" style={{ margin: 0 }}>
            As a brand, you will gain access to countless streamers who are
            willing to advertise your products on their channels.{' '}
          </p>
          <p className="lime-text" style={{ margin: '35px 0' }}>
            Gain the exposure you need to grow your brand, and watch your
            returns explode.
          </p>
          <p className="white-text" style={{ margin: 0 }}>
            All you need to do is sign up, and our advanced Smart Contracts will
            allow you to advertise your products on thousands of channels
            simultaneously!
          </p>
        </div>
        <div className="section">
          <div className="section-header">
            <img src={`${streamIcon}`} alt="Brands" />
            <p className="section-title">STREAMERS</p>
          </div>
          <p className="white-text" style={{ margin: 0 }}>
            MOGUL is the ideal tool for you to increase your recognition in the
            market, expand your channel to new heights, and turn your passion
            into your lifestyle.
          </p>
          <p className="lime-text" style={{ margin: '40px 0 28px 0' }}>
            Connect with countless brands
          </p>
          <p className="lime-text" style={{ marginBottom: 28 }}>
            Create your personal token
          </p>
          <p className="lime-text" style={{ margin: 0 }}>
            Receive subscriptions and donations from your fans
          </p>
        </div>
        <div className="section">
          <div className="section-header">
            <img src={`${fanIcon}`} alt="Brands" />
            <p className="section-title">FANS</p>
          </div>
          <p className="white-text" style={{ margin: 0, marginBottom: 30 }}>
            At MOGUL, you will not only be able to enjoy your favorite content
            from the streamers you follow, but also support them by subscribing
            for premium content and donating to their channel using custom
            tokens.
          </p>
          <p className="lime-text" style={{ margin: 0 }}>
            Your support will mean that the streamers can drive up the frequency
            with which they stream and increase the quality of the content in
            many ways.
          </p>
        </div>
      </div>

      <div style={{ position: 'relative' }}>
        <h1
          className="text-separator left central-side"
          style={{ marginTop: 120, marginBottom: 60 }}
        >
          UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING
          THE POWER OF NFT -{' '}
        </h1>

        <h1
          className="text-separator left left-side"
          style={{
            position: 'absolute',
            top: 0,
            left: -2790,
            marginTop: 120,
          }}
        >
          UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING
          THE POWER OF NFT -{' '}
        </h1>

        <h1
          className="text-separator left right-side"
          style={{
            position: 'absolute',
            top: 0,
            left: 2790,
            marginTop: 120,
          }}
        >
          UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING
          THE POWER OF NFT -{' '}
        </h1>
      </div>

      <div style={{ position: 'relative' }}>
        <h1
          className="text-separator right central-side"
          style={{ marginBottom: 120 }}
        >
          UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING
          THE POWER OF NFT -{' '}
        </h1>

        <h1
          className="text-separator right left-side"
          style={{
            position: 'absolute',
            top: 0,
            left: -2790,
          }}
        >
          UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING
          THE POWER OF NFT -{' '}
        </h1>

        <h1
          className="text-separator right right-side"
          style={{
            position: 'absolute',
            top: 0,
            left: 2790,
          }}
        >
          UTILIZING THE POWER OF NFT - UTILIZING THE POWER OF NFT - UTILIZING
          THE POWER OF NFT -{' '}
        </h1>
      </div>
    </div>
  )
}

export default Header
