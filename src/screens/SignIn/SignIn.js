import React, { useState } from 'react'

import './SignIn.css'
import { useHistory } from 'react-router-dom'
import background from '../../assets/raster/header-background.png'
import logo from '../../assets/vector/logo.svg'
import { PrimaryButton, TextInput } from '../../components'
import rightArrow from '../../assets/vector/right-lime-arrow.svg'
import floorTint from '../../assets/raster/floor-tint.png'
// import limeLine from '../../assets/raster/lime-line.png'

const SignIn = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [emailValidity, setEmailValidity] = useState(null)
  const [passwordValidity, setPasswordValidity] = useState(null)

  const history = useHistory()

  const isEmail = (_email) => {
    // eslint-disable-next-line no-useless-escape
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    return re.test(String(_email).toLowerCase())
  }

  const checkValidity = () => {
    let valid = true

    if (!isEmail(email)) {
      setEmailValidity('Invalid email address')
      valid = false
    } else {
      setEmailValidity(null)
    }

    if (password === '') {
      setPasswordValidity('Invalid password')
      valid = false
    } else {
      setPasswordValidity(null)
    }

    return valid
  }

  const handleOnSubmit = (e) => {
    // e.preventDefault()

    if (checkValidity()) history.push('/dashboard')
    else e.preventDefault()
  }

  return (
    <div
      className="sign-in-screen"
      style={{ backgroundImage: `url(${background})`, display: 'flex' }}
    >
      <div
        className="animated-floor"
        style={{ height: 647, top: 'auto', bottom: 0 }}
      />
      <img
        src={floorTint}
        alt="Tint"
        style={{
          position: 'absolute',
          bottom: 0,
          height: 330,
          width: '100%',
        }}
      />

      <div className="sign-in-panel">
        <div>
          <img src={logo} alt="Logo" />
          <h2
            className="title-big"
            style={{ margin: '80px 0 0 0', width: '70%', textAlign: 'left' }}
          >
            Join the revolution!
          </h2>
          <h3
            className="pink-text"
            style={{ margin: '20px 0 80px 0', fontSize: 24 }}
          >
            Instantly. Safe. Reliable.
          </h3>

          <form onSubmit={handleOnSubmit}>
            <TextInput
              placeholder="E-mail address"
              styleId={4}
              onChange={(e) => setEmail(e.target.value)}
              additionalStyle={{ margin: '0 0 5px 0' }}
            />

            <p className="pink-text" style={{ margin: '0 0 40px 0' }}>
              {emailValidity}
            </p>

            <TextInput
              placeholder="Password"
              styleId={4}
              additionalStyle={{ margin: '40px 0 0 0' }}
              onChange={(e) => setPassword(e.target.value)}
            />

            <p className="pink-text" style={{ margin: '0 0 40px 0' }}>
              {passwordValidity}
            </p>

            <div
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <PrimaryButton
                icon={rightArrow}
                textContent="SIGN IN"
                additionalStyle={{ marginTop: 40 }}
                type="submit"
              />
            </div>
          </form>
        </div>
        <div style={{ marginTop: 80 }}>
          <a href="/#" className="bottom-sign">
            Forgot your password?
          </a>
          <p className="bottom-sign" style={{ margin: '40px 0 0 0' }}>
            Don’t have an account?{' '}
            <a
              href="/register"
              style={{ color: '#D10286', textDecoration: 'underline' }}
            >
              Sign up!
            </a>
          </p>
        </div>
      </div>
      <div className="sign-in-greeting">
        <h1 className="title-huge">MAJOR ICO EVENT of 2021</h1>
        <h3 className="subtitle-lime">
          Welcome to the hottest hub for the streaming community
        </h3>
        <h4 className="subtitle-white">
          introducing the MOGUL NFT Marketplace
        </h4>
      </div>
    </div>
  )
}

export default SignIn
