import React, { useState } from 'react'

import './WalletAccessMethod.css'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  AccessMethod,
  AdminPanelNavigation,
  AdminPanelSteps,
} from '../../../../components'

const WalletAccessMethod = ({ setCurrentStep }) => {
  const [isAccessMethodActive, setIsAccessMethodActive] = useState({
    keystore: true,
    mnemonicPhrase: false,
    privKey: false,
  })

  const history = useHistory()

  return (
    <>
      <AdminPanelSteps
        steps={['Wallet import method', 'Access']}
        activeStepIndex={0}
      />

      <div className="wallet-access-method">
        <p className="admin-panel-title">
          choose ethereum wallet access method
        </p>
        <p className="lime-text" style={{ margin: '40px 0 80px 0' }}>
          The tokens purchased from us will be sent to the provided wallet.
        </p>

        <div className="access-methods">
          <AccessMethod
            text="KEYSTORE FILE"
            isActive={isAccessMethodActive.keystore}
            handleOnClick={() =>
              setIsAccessMethodActive({
                keystore: true,
                mnemonicPhrase: false,
                privKey: false,
              })
            }
            additionalStyles={{ marginRight: 40 }}
          />
          <AccessMethod
            text="MNEMONIC PHRASE"
            isActive={isAccessMethodActive.mnemonicPhrase}
            handleOnClick={() => {
              setIsAccessMethodActive({
                keystore: false,
                mnemonicPhrase: true,
                privKey: false,
              })
            }}
            additionalStyles={{ marginRight: 40 }}
          />
          <AccessMethod
            text="PRIVATE KEY"
            isActive={isAccessMethodActive.privKey}
            handleOnClick={() =>
              setIsAccessMethodActive({
                keystore: false,
                mnemonicPhrase: false,
                privKey: true,
              })
            }
          />
        </div>
      </div>

      <AdminPanelNavigation
        buttonsLeft={[
          {
            label: 'Back',
            onClick: () => history.push('/create-or-import-wallet'),
          },
        ]}
        buttonsRight={[
          {
            label: 'Next',
            onClick: () => {
              if (isAccessMethodActive.keystore) {
                setCurrentStep(1.1)
              } else if (isAccessMethodActive.mnemonicPhrase) {
                setCurrentStep(1.2)
              } else if (isAccessMethodActive.privKey) {
                setCurrentStep(1.3)
              }
            },
          },
        ]}
      />
    </>
  )
}

export default WalletAccessMethod

WalletAccessMethod.propTypes = {
  setCurrentStep: PropTypes.func,
}
