import React, { useState, useEffect } from 'react'

import './MnemonicPhraseAccess.css'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  TextInput,
} from '../../../../components'
import CheckBoxInput from '../../../../components/CheckBoxInput/CheckBoxInput'

const MnemonicPhraseAccess = ({ setCurrentStep }) => {
  const [is12OptionChecked, setIs12OptionChecked] = useState(true)
  const [isNoOptionChecked, setIsNoOptionChecked] = useState(true)

  const [textInputs, setTextInputs] = useState([])

  const [extraWord, setExtraWord] = useState('')
  const [extraWordValidity, setExtraWordValidity] = useState(null)

  const history = useHistory()

  const checkValidity = () => {
    let valid = true

    if (!isNoOptionChecked && extraWord === '') {
      setExtraWordValidity('Invalid extra word')
      valid = false
    } else {
      setExtraWordValidity(null)
    }

    return valid
  }

  function generateTextInputs() {
    const out = []

    if (!is12OptionChecked) {
      for (let i = 0; i < 12; i += 1) {
        out.push(
          <TextInput
            styleId={4}
            precedingText={`${i + 1}.`}
            additionalStyle={{ margin: 0 }}
          />,
        )
      }
    } else {
      for (let i = 0; i < 24; i += 1) {
        out.push(
          <TextInput
            styleId={4}
            precedingText={`${i + 1}.`}
            additionalStyle={{ margin: 0 }}
          />,
        )
      }
    }

    setTextInputs(out)
  }

  useEffect(() => {
    const out = []

    for (let i = 0; i < 12; i += 1) {
      out.push(
        <TextInput
          styleId={4}
          precedingText={`${i + 1}.`}
          additionalStyle={{ margin: 0 }}
        />,
      )
    }

    setTextInputs(out)
  }, [])

  return (
    <>
      <AdminPanelSteps
        steps={['Wallet import method', 'Access']}
        activeStepIndex={1}
      />

      <form
        className="mnemonic-phrase-access"
        onSubmit={() => history.push('/dashboard')}
      >
        <p className="admin-panel-title">access by mnemonic phrase</p>
        <p className="lime-text" style={{ margin: '40px 0 10px 0' }}>
          Please type in your mnemonic phrase.
        </p>
        <div
          style={{
            display: 'flex',
          }}
        >
          <CheckBoxInput
            label={<p className="checkbox-label">12 value</p>}
            isChecked={is12OptionChecked}
            handleOnCheck={() => {
              setIs12OptionChecked(true)
              generateTextInputs()
            }}
            id="is12OptionChecked"
            additionalStyles={{ marginRight: 40 }}
          />

          <CheckBoxInput
            label={<p className="checkbox-label">24 value</p>}
            isChecked={!is12OptionChecked}
            handleOnCheck={() => {
              setIs12OptionChecked(false)
              generateTextInputs()
            }}
            id="is24OptionChecked"
          />
        </div>
        <div
          className={
            is12OptionChecked
              ? 'text-inputs-container'
              : 'text-inputs-container-expanded'
          }
          style={{ marginTop: 30 }}
        >
          {textInputs}
        </div>

        <p className="admin-panel-title" style={{ marginTop: 80 }}>
          DO YOU HAVE AN EXTRA WORD?
        </p>

        <div style={{ display: 'flex', alignItems: 'center', marginTop: 30 }}>
          <div
            style={{
              display: 'flex',
              marginRight: 80,
            }}
          >
            <CheckBoxInput
              label={<p className="checkbox-label">No</p>}
              isChecked={isNoOptionChecked}
              handleOnCheck={() => {
                setExtraWordValidity(null)
                setIsNoOptionChecked(true)
              }}
              id="isNoOptionChecked"
              additionalStyles={{ marginRight: 40 }}
            />

            <CheckBoxInput
              label={<p className="checkbox-label">Yes</p>}
              isChecked={!isNoOptionChecked}
              handleOnCheck={() => {
                setIsNoOptionChecked(false)
              }}
              id="isYesOptionChecked(false)"
            />
          </div>
          <div style={{ width: '44%' }}>
            <TextInput
              styleId={4}
              title="YOUR EXTRA WORD"
              isPassword
              additionalStyle={{ margin: 0 }}
              disabled={isNoOptionChecked}
              onChange={(e) => setExtraWord(e.target.value)}
              value={extraWord}
            />
            {extraWordValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {extraWordValidity}
              </p>
            ) : null}
          </div>
        </div>
      </form>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'ACCESS WALLET',
            onClick: (e) => {
              if (!checkValidity()) e.preventDefault()
            },
            type: 'submit',
          },
        ]}
        buttonsLeft={[
          {
            label: 'BACK',
            onClick: () => {
              setCurrentStep(0)
            },
          },
        ]}
      />
    </>
  )
}
export default MnemonicPhraseAccess

MnemonicPhraseAccess.propTypes = {
  setCurrentStep: PropTypes.func,
}
