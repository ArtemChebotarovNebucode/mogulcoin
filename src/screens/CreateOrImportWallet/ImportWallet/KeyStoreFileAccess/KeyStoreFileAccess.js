import React, { useState } from 'react'

import './KeyStoreFileAccess.css'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  FileInput,
  TextInput,
} from '../../../../components'

const KeyStoreFileAccess = ({ setCurrentStep }) => {
  const history = useHistory()

  const [keyStoreFile, setKeyStoreFile] = useState([])
  const [filePassword, setFilePassword] = useState('')

  const [keyStoreFileValidity, setKeyStoreFileValidity] = useState(null)
  const [filePasswordValidity, setFilePasswordValidity] = useState(false)

  const checkValidity = () => {
    let valid = true

    if (filePassword === '') {
      setFilePasswordValidity('Invalid password')
      valid = false
    } else {
      setFilePasswordValidity(null)
    }

    if (keyStoreFile.length === 0) {
      setKeyStoreFileValidity(true)
      valid = false
    } else {
      setKeyStoreFileValidity(false)
    }

    return valid
  }

  return (
    <>
      <AdminPanelSteps
        steps={['Wallet import method', 'Access']}
        activeStepIndex={1}
      />

      <form
        className="key-store-file-access"
        onSubmit={() => history.push('/dashboard')}
      >
        <div style={{ width: '45%' }}>
          <p className="admin-panel-title">access by keystore file</p>
          <p className="lime-text" style={{ margin: '40px 0' }}>
            The tokens purchased from us will be sent to the provided wallet.
          </p>
          <div>
            <TextInput
              styleId={4}
              isPassword
              title="FILE PASSWORD *"
              additionalStyle={{ margin: 0 }}
              onChange={(e) => setFilePassword(e.target.value)}
            />
            {filePasswordValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {filePasswordValidity}
              </p>
            ) : null}
          </div>
        </div>
        <div style={{ width: '45%' }}>
          <FileInput
            title="Upload keystore file"
            instruction="Drag a file here or Select a file"
            files={keyStoreFile}
            setFiles={setKeyStoreFile}
            invalid={keyStoreFileValidity}
          />
        </div>
      </form>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'ACCESS WALLET',
            onClick: (e) => {
              if (!checkValidity()) e.preventDefault()
            },
            type: 'submit',
          },
        ]}
        buttonsLeft={[
          {
            label: 'BACK',
            onClick: () => {
              setCurrentStep(0)
            },
          },
        ]}
      />
    </>
  )
}

export default KeyStoreFileAccess

KeyStoreFileAccess.propTypes = {
  setCurrentStep: PropTypes.func,
}
