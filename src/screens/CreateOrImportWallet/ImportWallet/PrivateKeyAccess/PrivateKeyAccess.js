import React, { useState } from 'react'

import './PrivateKeyAccess.css'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  TextInput,
} from '../../../../components'

// eslint-disable-next-line no-unused-vars
const PrivateKeyAccess = ({ setCurrentStep }) => {
  const history = useHistory()

  const [filePassword, setFilePassword] = useState('')
  const [filePasswordValidity, setFilePasswordValidity] = useState(null)

  const checkValidity = () => {
    let valid = true

    if (filePassword === '') {
      setFilePasswordValidity('Invalid password')
      valid = false
    } else {
      setFilePasswordValidity(null)
    }

    return valid
  }

  return (
    <>
      <AdminPanelSteps
        steps={['Wallet import method', 'Access']}
        activeStepIndex={1}
      />

      <form
        className="private-key-access"
        onSubmit={() => history.push('/dashboard')}
      >
        <p className="admin-panel-title">access by private key</p>
        <p className="lime-text" style={{ margin: '40px 0' }}>
          Enter your private key for the wallet you want to access.
        </p>

        <div>
          <TextInput
            styleId={4}
            title="FILE PASSWORD *"
            isPassword
            additionalStyle={{ margin: 0 }}
            value={filePassword}
            onChange={(e) => setFilePassword(e.target.value)}
          />
          {filePasswordValidity ? (
            <p className="pink-text" style={{ margin: 0 }}>
              {filePasswordValidity}
            </p>
          ) : null}
        </div>
      </form>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'ACCESS WALLET',
            onClick: (e) => {
              if (!checkValidity()) e.preventDefault()
            },
            type: 'submit',
          },
        ]}
        buttonsLeft={[
          {
            label: 'BACK',
            onClick: () => {
              setCurrentStep(0)
            },
          },
        ]}
      />
    </>
  )
}
export default PrivateKeyAccess

PrivateKeyAccess.propTypes = {
  setCurrentStep: PropTypes.func,
}
