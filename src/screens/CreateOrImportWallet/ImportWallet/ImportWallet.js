import React, { useState } from 'react'

import { NavBar } from '../../../components'
import background from '../../../assets/raster/admin-background.png'
import WalletAccessMethod from './WalletAccessMethod/WalletAccessMethod'
import MnemonicPhraseAccess from './MnemonicPhraseAccess/MnemonicPhraseAccess'
import KeyStoreFileAccess from './KeyStoreFileAccess/KeyStoreFileAccess'
import PrivateKeyAccess from './PrivateKeyAccess/PrivateKeyAccess'

const ImportWallet = () => {
  const [currentStep, setCurrentStep] = useState(0)

  return (
    <div
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <NavBar isSignedIn isExtended />

      {currentStep === 0 ? (
        <WalletAccessMethod setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 1.1 ? (
        <KeyStoreFileAccess setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 1.2 ? (
        <MnemonicPhraseAccess setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 1.3 ? (
        <PrivateKeyAccess setCurrentStep={setCurrentStep} />
      ) : null}
    </div>
  )
}
export default ImportWallet
