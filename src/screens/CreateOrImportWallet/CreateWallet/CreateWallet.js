import React, { useState } from 'react'

import { NavBar } from '../../../components'
import background from '../../../assets/raster/admin-background.png'
import WalletSetup from './WalletSetup/WalletSetup'
import FinishWalletSetup from './FinishWalletSetup/FinishWalletSetup'

const CreateWallet = () => {
  const [currentStep, setCurrentStep] = useState(0)

  return (
    <form
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <NavBar isSignedIn isExtended />

      {currentStep === 0 ? (
        <WalletSetup setCurrentStep={setCurrentStep} />
      ) : null}

      {currentStep === 1 ? (
        <FinishWalletSetup setCurrentStep={setCurrentStep} />
      ) : null}
    </form>
  )
}
export default CreateWallet
