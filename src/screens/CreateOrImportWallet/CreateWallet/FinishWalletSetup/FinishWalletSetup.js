import React from 'react'

import './FinishWalletSetup.css'
import { useHistory } from 'react-router-dom'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  TextInput,
} from '../../../../components'
import acceptanceIcon from '../../../../assets/vector/acceptance-icon.svg'

const FinishWalletSetup = () => {
  const history = useHistory()

  return (
    <>
      <AdminPanelSteps
        steps={['Password creation', 'Summary']}
        activeStepIndex={1}
      />

      <div className="finish-setup-panel">
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <img src={acceptanceIcon} alt="Acceptance" />
          <p
            className="admin-panel-title"
            style={{ marginBottom: 0, marginLeft: 24 }}
          >
            your ethereum wallet has been set up!
          </p>
        </div>
        <p className="lime-text" style={{ margin: '40px 0' }}>
          This data entitles you to manage funds accumulated in the wallet.
        </p>

        <TextInput
          title="PUBLIC KEY"
          styleId={4}
          additionalStyle={{ margin: '0 0 40px 0' }}
          isPassword
          withoutLookup
        />
        <TextInput
          title="WALLET PASSWORD"
          styleId={4}
          additionalStyle={{ margin: '0 0 95px 0' }}
          isPassword
          withoutLookup
        />

        <div style={{ display: 'flex' }}>
          <div className="wallet-gradient-container">
            <p className="intro-title">DON’T LOSE IT</p>
            <p className="pink-text">
              In the event of loss of access data, it is not possible to regain
              access to the wallet and funds accumulated on it.{' '}
            </p>
          </div>

          <div
            className="wallet-gradient-container"
            style={{ margin: '0 40px' }}
          >
            <p className="intro-title">DON’T SHARE IT</p>
            <p className="pink-text">
              Keep your Ethereum wallet access data and do not share it with
              anyone!
            </p>
          </div>

          <div className="wallet-gradient-container">
            <p className="intro-title">MAKE A BACKUP</p>
            <p className="pink-text">
              We do not store this data for security reasons.
            </p>
          </div>
        </div>
      </div>

      <AdminPanelNavigation
        buttonsRight={[
          {
            label: 'finish without downloading',
            onClick: () => history.push('/dashboard'),
          },
          {
            label: 'DOWNLOAD KEYSTORE FILE',
            onClick: () => {},
          },
        ]}
      />
    </>
  )
}
export default FinishWalletSetup
