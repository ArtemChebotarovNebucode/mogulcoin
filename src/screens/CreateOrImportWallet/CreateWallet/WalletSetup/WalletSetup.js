import React, { useState } from 'react'

import './WalletSetup.css'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  AdminPanelNavigation,
  AdminPanelSteps,
  TextInput,
} from '../../../../components'

const WalletSetup = ({ setCurrentStep }) => {
  const history = useHistory()

  const [password, setPassword] = useState('')
  // eslint-disable-next-line no-unused-vars
  const [repeatPassword, setRepeatPassword] = useState('')
  const [isStrongPassword, setIsStrongPassword] = useState(false)

  const [passwordValidity, setPasswordValidity] = useState(null)
  const [repeatPasswordValidity, setRepeatPasswordValidity] = useState(null)

  const strongRegex = new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})',
  )

  const verifyPasswordStrength = () => {
    if (password.match(strongRegex)) {
      setIsStrongPassword(true)
    } else {
      setIsStrongPassword(false)
    }
  }

  const checkValidity = () => {
    let valid = true

    if (password === '') {
      setPasswordValidity('Invalid password')
      valid = false
    } else {
      setPasswordValidity(null)
    }

    if (repeatPassword !== password) {
      setRepeatPasswordValidity('Password mismatch')
      valid = false
    } else {
      setRepeatPasswordValidity(null)
    }

    return valid
  }

  return (
    <>
      <AdminPanelSteps
        steps={['Password creation', 'Summary']}
        activeStepIndex={0}
      />

      <div className="wallet-setup-panel">
        <p className="admin-panel-title">CREATE YOUR WALLET</p>
        <p className="lime-text" style={{ margin: '40px 0' }}>
          The password that will be provided when creating the wallet will be
          necessary in the future to access the newly created wallet using the
          keystore file. Do not lose or give this password to anyone, because in
          combination with a keystore file it allows you to manage the funds
          accumulated in your wallet. The private key also gives you full access
          to your wallet, so keep it in a safe place.
        </p>
        <p className="white-text" style={{ marginBottom: 40 }}>
          <span style={{ color: '#D10286', textDecoration: 'underline' }}>
            DO NOT FORGET
          </span>{' '}
          to save your password. You will need this{' '}
          <span style={{ color: '#D10286', textDecoration: 'underline' }}>
            Password + Keystore File
          </span>{' '}
          to unlock your wallet.
        </p>
        <div style={{ width: '50%' }}>
          <div>
            <TextInput
              styleId={4}
              title="WALLET PASSWORD *"
              isPassword
              placeholder="Enter At Least 9 Characters"
              additionalStyle={{ margin: 0 }}
              onChange={(e) => {
                setPassword(e.target.value)
                verifyPasswordStrength()
              }}
            />
            {passwordValidity ? (
              <p className="pink-text" style={{ margin: 0 }}>
                {passwordValidity}
              </p>
            ) : null}
          </div>
          <p
            className="white-text"
            style={{ fontSize: 14, margin: '10px 0 0 0' }}
          >
            Password strength:{' '}
            {isStrongPassword ? (
              <span style={{ color: '#54F7C5' }}>Good</span>
            ) : (
              'Weak'
            )}
          </p>
          <div>
            <TextInput
              styleId={4}
              title="WALLET PASSWORD *"
              isPassword
              placeholder="Repeat password*"
              additionalStyle={{ margin: '40px 0 0 0' }}
              onChange={(e) => setRepeatPassword(e.target.value)}
            />
            {repeatPasswordValidity ? (
              <p className="pink-text" style={{ margin: '0 0 40px 0' }}>
                {repeatPasswordValidity}
              </p>
            ) : null}
          </div>
          <p className="white-text" style={{ fontSize: 14, marginLeft: 0 }}>
            Already have an Ethereum wallet?{' '}
            <a
              style={{ color: '#D10286', textDecoration: 'underline' }}
              href="/import-wallet"
            >
              Import here
            </a>
          </p>
        </div>
      </div>

      <AdminPanelNavigation
        buttonsLeft={[
          {
            label: 'BACK',
            onClick: () => {
              history.push('create-or-import-wallet')
            },
          },
        ]}
        buttonsRight={[
          {
            label: 'CREATE WALLET',
            onClick: (e) => {
              if (checkValidity()) setCurrentStep(1)
              else e.preventDefault()
            },
            type: 'submit',
          },
        ]}
      />
    </>
  )
}

export default WalletSetup

WalletSetup.propTypes = {
  setCurrentStep: PropTypes.func,
}
