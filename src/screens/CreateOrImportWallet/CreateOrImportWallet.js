import React from 'react'

import './CreateOrImportWallet.css'
import { useHistory } from 'react-router-dom'
import { AdminPanelNavigation, NavBar } from '../../components'
import background from '../../assets/raster/admin-background.png'

const CreateOrImportWallet = () => {
  const history = useHistory()

  return (
    <div
      className="admin-screen"
      style={{
        backgroundImage: `url(${background})`,
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <NavBar isSignedIn isExtended />
      <div className="create-or-import-wallet">
        <div>
          <p className="admin-panel-title" style={{ marginBottom: 40 }}>
            are you an owner of ethereum wallet?
          </p>
          <p className="lime-text" style={{ marginLeft: 0 }}>
            The tokens purchased from us we send to the Ethereum wallet. Do you
            want us to set up a new wallet or do you already have one?
          </p>
        </div>
      </div>

      <AdminPanelNavigation
        buttonsLeft={[
          {
            label: 'BACK',
            onClick: () => {
              // history does not reload page @@bug
              window.location.href = '/dashboard'
            },
          },
        ]}
        buttonsRight={[
          {
            label: 'Yes, i have one',
            onClick: () => {
              history.push('/import-wallet')
            },
          },
          {
            label: 'no, set up ethereum wallet',
            onClick: () => history.push('/create-wallet'),
          },
        ]}
      />
    </div>
  )
}

export default CreateOrImportWallet
