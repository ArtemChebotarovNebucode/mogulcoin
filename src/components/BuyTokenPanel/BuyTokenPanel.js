import React, { useEffect, useState } from 'react'

import './BuyTokenPanel.css'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import CryptoButton from '../CryptoButton/CryptoButton'
import rightArrow from '../../assets/vector/right-lime-arrow.svg'
import { PrimaryButton, TextInput } from '../index'

const BuyTokenPanel = ({ isExtended }) => {
  const [isCryptoActive, setIsCryptoActive] = useState({
    btc: true,
    eth: false,
    usdt: false,
    usdc: false,
  })

  const [currencyLabel, setCurrencyLabel] = useState('BTC')

  useEffect(() => {
    if (isCryptoActive.btc) setCurrencyLabel('BTC')
    else if (isCryptoActive.eth) setCurrencyLabel('ETH')
    else if (isCryptoActive.usdt) setCurrencyLabel('USDT')
    else if (isCryptoActive.usdc) setCurrencyLabel('USDC')
  }, [isCryptoActive])

  const history = useHistory()

  return (
    <form className="buy-token-panel">
      <h2 className="admin-panel-title">buy our tokens</h2>
      <p className="admin-panel-pink-text" style={{ margin: '16px 0 40px 0' }}>
        Instantly. Safe. Reliable.
      </p>
      <div className="crypto-buttons" style={{ marginBottom: 40 }}>
        <CryptoButton
          text="BTC"
          isActive={isCryptoActive.btc}
          handleOnClick={() =>
            setIsCryptoActive({
              btc: !isCryptoActive.btc,
              eth: false,
              usdt: false,
              usdc: false,
            })
          }
        />
        <CryptoButton
          text="ETH"
          isActive={isCryptoActive.eth}
          handleOnClick={() =>
            setIsCryptoActive({
              btc: false,
              eth: !isCryptoActive.eth,
              usdt: false,
              usdc: false,
            })
          }
        />
        <CryptoButton
          text="USDT"
          isActive={isCryptoActive.usdt}
          handleOnClick={() =>
            setIsCryptoActive({
              btc: false,
              eth: false,
              usdt: !isCryptoActive.usdt,
              usdc: false,
            })
          }
        />
        <CryptoButton
          text="USDC"
          isActive={isCryptoActive.usdc}
          handleOnClick={() =>
            setIsCryptoActive({
              btc: false,
              eth: false,
              usdt: false,
              usdc: !isCryptoActive.usdc,
            })
          }
        />
      </div>
      <TextInput
        styleId={2}
        title="You pay"
        insertion={currencyLabel}
        value="50.00"
        additionalStyle={
          !isExtended ? { marginBottom: 40 } : { marginBottom: 15 }
        }
        type="number"
      />

      {isExtended ? (
        <p
          className="pink-text"
          style={{ fontSize: 12, marginBottom: 40, marginTop: 0 }}
        >
          equals 114 221.00 $
        </p>
      ) : null}

      <TextInput
        styleId={2}
        title="You get"
        insertion="MOGUL COINS"
        value="50.00"
        additionalStyle={{ marginBottom: 40 }}
        type="number"
      />

      {isExtended ? (
        <>
          <TextInput
            styleId={2}
            title="I RECEIVED PURCHASED TOKENS ON:"
            value="0x0225da77813bff1c205b629232"
            additionalStyle={{ marginBottom: 15 }}
            isCopyingAllowed
          />
          <a
            className="white-text"
            style={{ textDecoration: 'underline', marginBottom: 40 }}
            href="/#"
          >
            Change wallet
          </a>
        </>
      ) : null}

      <PrimaryButton
        icon={rightArrow}
        textContent={!isExtended ? 'INSTALL WALLET TO BUY' : 'BUY NOW'}
        additionalStyle={{ alignSelf: 'flex-end' }}
        onClick={() => history.push('/payment/pay-with-provider')}
      />
    </form>
  )
}

export default BuyTokenPanel

BuyTokenPanel.propTypes = {
  isExtended: PropTypes.bool,
}
