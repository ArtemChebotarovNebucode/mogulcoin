import React, { useEffect, useState } from 'react'

import './PrimaryButton.css'
import * as PropTypes from 'prop-types'

const PrimaryButton = ({
  textContent,
  icon,
  onClick,
  additionalStyle,
  id,
  iconPositionIsLeft,
  type,
}) => {
  const [spanMargin, setSpanMargin] = useState({})

  useEffect(() => {
    if (icon && iconPositionIsLeft) {
      setSpanMargin({ marginLeft: 10 })
    } else if (icon && !iconPositionIsLeft) {
      setSpanMargin({ marginRight: 10 })
    }
  }, [])

  return (
    <button
      className="primary-button"
      type={type === 'submit' ? 'submit' : 'button'}
      onClick={onClick}
      style={additionalStyle}
      id={id}
    >
      {icon && iconPositionIsLeft ? <img src={icon} alt="Icon" /> : null}
      <span style={spanMargin}>{textContent}</span>
      {icon && !iconPositionIsLeft ? <img src={icon} alt="Icon" /> : null}
    </button>
  )
}

export default PrimaryButton

PrimaryButton.propTypes = {
  textContent: PropTypes.string,
  icon: PropTypes.string, // path to icon
  onClick: PropTypes.func,
  additionalStyle: PropTypes.object,
  id: PropTypes.string,
  iconPositionIsLeft: PropTypes.bool,
  type: PropTypes.string,
}
