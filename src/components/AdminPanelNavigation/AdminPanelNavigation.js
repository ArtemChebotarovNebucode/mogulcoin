import React from 'react'

import './AdminPanelNavigation.css'
import PropTypes from 'prop-types'
import { PrimaryButton } from '../index'

const AdminPanelNavigation = ({
  buttonsLeft,
  buttonsRight,
  additionalStyles,
}) => (
  <div className="create-wallet-navigation" style={additionalStyles}>
    <div style={{ display: 'flex' }}>
      {buttonsLeft
        ? buttonsLeft.map((button, index) => (
            <PrimaryButton
              textContent={button.label}
              additionalStyle={
                index !== buttonsLeft.length - 1 ? { marginRight: 40 } : {}
              }
              onClick={button.onClick}
              type={button.type}
            />
          ))
        : null}
    </div>
    <div style={{ display: 'flex' }}>
      {buttonsRight
        ? buttonsRight.map((button, index) => (
            <PrimaryButton
              textContent={button.label}
              additionalStyle={{
                ...(index !== buttonsRight.length - 1
                  ? { marginRight: 40 }
                  : {}),
                ...(button.label === 'finish without downloading'
                  ? { border: 'none' }
                  : {}),
              }}
              onClick={button.onClick}
              type={button.type}
            />
          ))
        : null}
    </div>
  </div>
)

export default AdminPanelNavigation

AdminPanelNavigation.propTypes = {
  buttonsLeft: PropTypes.array, // array of button names placed on the left side
  buttonsRight: PropTypes.array, // array of button names placed on the right side
  additionalStyles: PropTypes.object,
}
