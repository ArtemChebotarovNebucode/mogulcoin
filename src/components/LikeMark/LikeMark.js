import React from 'react'

import './LikeMark.css'

import PropTypes from 'prop-types'
import likeGreen from '../../assets/vector/like-green-icon.svg'
import likeRed from '../../assets/vector/like-red-icon.svg'
import { useMain } from '../../contexts/MainProvider/MainProvider'

const LikeMark = ({ positive }) => {
  const { windowWidth } = useMain()

  return (
    <>
      {windowWidth > 847 ? (
        <div className={`like-mark ${positive ? 'positive' : 'negative'}`}>
          <p
            className={`like-mark-text ${positive ? 'positive' : 'negative'}`}
            style={{ marginRight: 10 }}
          >
            {positive ? 'Yes' : 'No'}
          </p>

          <img
            src={`${positive ? likeGreen : likeRed}`}
            style={!positive ? { width: 20, height: 20 } : {}}
            alt="Like Mark"
          />
        </div>
      ) : (
        <img
          src={`${positive ? likeGreen : likeRed}`}
          style={!positive ? { width: 20, height: 20 } : {}}
          alt="Like Mark"
        />
      )}
    </>
  )
}

export default LikeMark

LikeMark.propTypes = {
  positive: PropTypes.bool, // is like or dislike
}
