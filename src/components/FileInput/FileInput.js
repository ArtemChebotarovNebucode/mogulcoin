import React, { useState } from 'react'

import './FileInput.css'
import PropTypes from 'prop-types'

const FileInput = ({
  title,
  subtitle,
  instruction,
  multiple,
  files,
  setFiles,
  invalid,
}) => {
  const [drag, setDrag] = useState(false)

  return (
    <div
      className={`file-input-container ${drag ? 'drag' : ''} ${
        invalid ? 'invalid' : ''
      }`}
      onDragOver={() => {
        setDrag(true)
      }}
      onDrop={() => setDrag(false)}
    >
      <div className="fake-upload">
        {files.length === 0 ? (
          <>
            <p className="file-input-title">{title}</p>
            {subtitle ? (
              <p className="file-input-subtitle">{subtitle}</p>
            ) : null}
            <p className="file-input-instruction">{instruction}</p>
          </>
        ) : (
          <>
            {files.map((file) => (
              <p className="file-input-instruction">{file.name}</p>
            ))}
          </>
        )}
      </div>
      <input
        type="file"
        name="upload"
        id="real-upload"
        className="real-upload"
        onChange={(e) => {
          setFiles(Array.from(e.target.files))
        }}
        multiple={multiple}
      />
    </div>
  )
}

export default FileInput

FileInput.propTypes = {
  title: PropTypes.string, // file input area text content line 1
  subtitle: PropTypes.string, // file input area text content line 2
  instruction: PropTypes.string, // file input area text content line 3
  multiple: PropTypes.bool,
  setFiles: PropTypes.func,
  files: PropTypes.array,
  invalid: PropTypes.bool,
}
