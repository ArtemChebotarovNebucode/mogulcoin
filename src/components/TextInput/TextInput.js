import React, { useState } from 'react'
import PropTypes from 'prop-types'

import './TextInput.css'

import sendIcon from '../../assets/vector/send-icon.svg'
import copyIcon from '../../assets/vector/copy-icon.svg'
import searchIcon from '../../assets/vector/search-icon.svg'
import eyeIcon from '../../assets/vector/eye-icon.svg'
import eyeIconDisabled from '../../assets/vector/eye-icon-disabled.svg'
import copyPinkIcon from '../../assets/vector/copy-pink-icon.svg'
import downArrow from '../../assets/vector/down-pink-arrow.svg'

import phoneNumberPrefixes from '../../data/phoneNumberPrefixes'

const TextInput = ({
  placeholder,
  id,
  styleId,
  title,
  insertion,
  value,
  isCopyingAllowed,
  isPassword,
  additionalStyle,
  precedingText,
  isPhoneNumber,
  onChange,
  type,
  withoutLookup,
  disabled,
}) => {
  const [inputValue, setInputValueChange] = useState(value || '')
  const [isValueHidden, setIsValueHidden] = useState(isPassword || false)
  const [isPrefixesDropDisplayed, setIsPrefixesDropDisplayed] = useState(false)
  const [chosenPrefix, setChosenPrefix] = useState('+48')

  if (styleId === 1) {
    return (
      <div className="text-input-wrap-1" id={id || ''}>
        <input
          type="text"
          className="text-input"
          placeholder={placeholder}
          value={inputValue}
          onChange={(e) => {
            setInputValueChange(e.target.value)
            if (onChange) {
              onChange(e)
            }
          }}
        />
        <button type="submit" className="send-button">
          <img src={`${sendIcon}`} alt="Send" />
        </button>
      </div>
    )
  }

  if (styleId === 2) {
    return (
      <div className="text-input-wrap-2" style={additionalStyle}>
        <p className="title">{title}</p>
        <div style={{ display: 'flex' }}>
          <input
            type={type || 'text'}
            className="text-input"
            value={inputValue}
            onChange={(e) => {
              setInputValueChange(e.target.value)
              if (onChange) {
                onChange(e)
              }
            }}
          />
          {!isCopyingAllowed ? (
            <p
              className="title"
              style={{ margin: 0, alignItems: 'center', display: 'flex' }}
            >
              {insertion}
            </p>
          ) : (
            <button
              className="button-wrapper"
              onClick={() => navigator.clipboard.writeText(inputValue)}
              type="button"
            >
              <img
                src={copyIcon}
                alt="Copy to clipboard"
                style={{ cursor: 'pointer' }}
              />
            </button>
          )}
        </div>
      </div>
    )
  }

  if (styleId === 3) {
    return (
      <div className="text-input-wrap-3" style={additionalStyle}>
        <input
          type="text"
          className="text-input"
          value={inputValue}
          onChange={(e) => {
            setInputValueChange(e.target.value)
            if (onChange) {
              onChange(e)
            }
          }}
          placeholder={placeholder}
        />
        <img
          src={searchIcon}
          alt="Search"
          className="search-button"
          style={{
            width: 12,
            height: 12,
          }}
        />
      </div>
    )
  }

  if (styleId === 4) {
    return (
      <div
        className="text-input-wrap-4"
        style={{
          ...additionalStyle,
          ...(isCopyingAllowed || isPassword ? { paddingRight: 23 } : {}),
          ...(disabled ? { border: '2px solid rgb(125, 110, 140)' } : {}),
        }}
      >
        {title ? (
          <p
            className="input-title"
            style={disabled ? { color: 'rgb(125, 110, 140)' } : {}}
          >
            {title}
          </p>
        ) : null}
        {precedingText ? (
          <p className="white-text" style={{ margin: '0 23px' }}>
            {precedingText}
          </p>
        ) : null}
        {isPhoneNumber ? (
          <div>
            <button
              style={{ position: 'relative', padding: 0 }}
              type="button"
              onClick={() => {
                setIsPrefixesDropDisplayed((prev) => !prev)
              }}
            >
              <div
                style={{
                  borderRight: '2px solid #D10286',
                  display: 'flex',
                  alignItems: 'center',
                  paddingLeft: 24,
                  height: 47,
                }}
              >
                <p
                  className="white-text"
                  style={{
                    margin: 0,
                    marginRight: 28,
                    whiteSpace: 'nowrap',
                    fontSize: 16,
                  }}
                >
                  {chosenPrefix}
                </p>
                <img
                  src={downArrow}
                  alt="Arrow"
                  style={{ height: 'fit-content', marginRight: 23 }}
                />
              </div>
            </button>

            <div
              className="phone-number-prefixes"
              style={{
                display: isPrefixesDropDisplayed ? 'block' : 'none',
              }}
            >
              {phoneNumberPrefixes.map((prefix) => (
                <button
                  className="white-text prefix-option"
                  style={{
                    margin: 0,
                    whiteSpace: 'nowrap',
                    lineHeight: '32px',
                    width: '100%',
                    textAlign: 'left',
                  }}
                  type="button"
                  onClick={() => {
                    setChosenPrefix(prefix.code)
                    onChange(prefix.code + inputValue)
                  }}
                >
                  {prefix.code} {prefix.name}
                </button>
              ))}
            </div>
          </div>
        ) : null}

        <input
          type={isValueHidden ? 'password' : 'text'}
          className="text-input"
          value={inputValue}
          onChange={(e) => {
            setInputValueChange(e.target.value)
            if (onChange && !isPhoneNumber) {
              onChange(e)
            } else {
              onChange(chosenPrefix + e.target.value)
            }
          }}
          placeholder={placeholder}
          disabled={disabled}
        />
        {isCopyingAllowed ? (
          <button
            className="button-wrapper"
            type="button"
            onClick={() => navigator.clipboard.writeText(inputValue)}
          >
            <img
              src={copyPinkIcon}
              alt="Copy"
              style={
                isCopyingAllowed && isPassword
                  ? { cursor: 'pointer', marginRight: 12 }
                  : { cursor: 'pointer' }
              }
            />
          </button>
        ) : null}
        {isPassword && !withoutLookup ? (
          <button
            type="button"
            className="button-wrapper"
            onClick={() => setIsValueHidden(!isValueHidden)}
          >
            <img
              src={disabled ? eyeIconDisabled : eyeIcon}
              alt="Show/Hide"
              style={{ cursor: 'pointer' }}
            />
          </button>
        ) : null}
      </div>
    )
  }

  return null
}

export default TextInput

TextInput.propTypes = {
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  id: PropTypes.string, // just a property like for each html tag
  styleId: PropTypes.number, // depending on styleId you can see different styled text inputs
  title: PropTypes.string, // kind of inscription for input to be more significant for user
  insertion: PropTypes.string, // inscription placed on right side of input,
  value: PropTypes.string,
  isCopyingAllowed: PropTypes.bool, // copy icon is showed (functionally) or not
  isPassword: PropTypes.bool, // is input value is hidden and eye icon (functionally) is showed
  additionalStyle: PropTypes.object,
  precedingText: PropTypes.string, // text insertion on the left side of input
  isPhoneNumber: PropTypes.bool, // is additional dropdown with phone number prefix choose possibility added
  type: PropTypes.string,
  withoutLookup: PropTypes.bool,
  disabled: PropTypes.bool,
}
