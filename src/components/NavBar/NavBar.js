import React from 'react'
import { useHistory } from 'react-router-dom'

import * as PropTypes from 'prop-types'
import logo from '../../assets/vector/logo.svg'
import collapsedMenuIcon from '../../assets/vector/collapsed-menu-icon.svg'

import './NavBar.css'

import { useMain } from '../../contexts/MainProvider/MainProvider'

const NavBar = ({
  // eslint-disable-next-line no-unused-vars
  isSignedIn,
  isExtended,
  nftRef,
  roadMapRef,
  teamRef,
  whyMogulRef,
}) => {
  const { windowWidth } = useMain()

  // eslint-disable-next-line no-unused-vars
  const history = useHistory()

  return (
    <div className="nav" style={{ justifyContent: 'space-between' }}>
      <a href="/#" id="logo" style={{ flexGrow: 0 }}>
        <img src={`${logo}`} alt="logo" />
      </a>
      {windowWidth > 1300 ? (
        <>
          <div
            className="tabs"
            style={{
              ...(isExtended ? { width: 821 } : { width: 507 }),
            }}
          >
            {isExtended ? (
              <>
                <a className="tab" href="/#">
                  Information
                </a>
                <a className="tab" href="/#">
                  MGL Token
                </a>
                <a className="tab" href="/#">
                  Invest in MGL
                </a>
                <a className="tab" href="/#">
                  Tokenomics
                </a>
                <a className="tab" href="/#">
                  Timeline
                </a>
                <a className="tab" href="/#">
                  Affiliate
                </a>
                <a className="tab" href="/#">
                  Legal
                </a>
              </>
            ) : (
              <>
                <button
                  className="tab"
                  type="button"
                  onClick={() => nftRef.current.scrollIntoView()}
                >
                  NFT Marketplace
                </button>
                <button
                  className="tab"
                  type="button"
                  onClick={() => whyMogulRef.current.scrollIntoView()}
                >
                  Why MOGUL?
                </button>
                <button
                  className="tab"
                  type="button"
                  onClick={() => roadMapRef.current.scrollIntoView()}
                >
                  Roadmap
                </button>
                <button
                  className="tab"
                  type="button"
                  onClick={() => teamRef.current.scrollIntoView()}
                >
                  Team
                </button>
                <button
                  className="tab"
                  type="button"
                  onClick={() =>
                    window.open('https://mogulcoin.medium.com/', '_blank')
                  }
                >
                  Blog
                </button>
              </>
            )}
          </div>
          {/* <div className="auth"> */}
          {/*  {!isSignedIn ? ( */}
          {/*    <> */}
          {/*      <a id="login" href="/login"> */}
          {/*        Login */}
          {/*      </a> */}
          {/*      <p className="gray-text">OR</p> */}
          {/*      <PrimaryButton */}
          {/*        textContent="Sign Up" */}
          {/*        onClick={() => history.push('/register')} */}
          {/*        additionalStyle={{ alignSelf: 'center' }} */}
          {/*      /> */}
          {/*    </> */}
          {/*  ) : ( */}
          {/*    <p className="email">philipp.siemek@gmail.com</p> */}
          {/*  )} */}
          {/* </div> */}
        </>
      ) : (
        <img src={collapsedMenuIcon} alt="Nav" id="collapsed-menu-icon" />
      )}
    </div>
  )
}

export default NavBar

NavBar.propTypes = {
  isSignedIn: PropTypes.bool, // to show currently logged in user
  isExtended: PropTypes.bool, // are extra tabs added
  nftRef: PropTypes.object,
  whyMogulRef: PropTypes.object,
  teamRef: PropTypes.object,
  roadMapRef: PropTypes.object,
}
