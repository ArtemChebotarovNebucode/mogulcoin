import React from 'react'

import './Pagination.css'

import PropTypes from 'prop-types'

const Pagination = ({ dataPerPage, totalData, paginate }) => {
  const pageNumbers = []

  for (let i = 1; i <= Math.ceil(totalData.length / dataPerPage); i += 1) {
    pageNumbers.push(i)
  }

  return (
    <ul className="pagination">
      {pageNumbers.map((number) => (
        <li key={number} className="page-item">
          <a
            href="!#"
            onClick={(e) => {
              e.preventDefault()
              paginate(number)
            }}
            className="page-link"
          >
            {number}
          </a>
        </li>
      ))}
    </ul>
  )
}

export default Pagination

Pagination.propTypes = {
  dataPerPage: PropTypes.number,
  totalData: PropTypes.array,
  paginate: PropTypes.func,
}
