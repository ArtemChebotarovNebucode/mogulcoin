import React from 'react'

import './CheckBoxInput.css'
import PropTypes from 'prop-types'

const CheckBoxInput = ({
  isChecked,
  handleOnCheck,
  id,
  label,
  additionalStyles,
  invalid,
}) => (
  <div className="checkbox-container" style={additionalStyles}>
    <input
      type="checkbox"
      id={id}
      checked={isChecked}
      onChange={(e) => handleOnCheck(e.target.checked)}
    />
    <label htmlFor={id} className={invalid ? 'invalid' : ''}>
      {label}
    </label>
  </div>
)

export default CheckBoxInput

CheckBoxInput.propTypes = {
  isChecked: PropTypes.bool,
  handleOnCheck: PropTypes.func,
  id: PropTypes.string,
  label: PropTypes.string,
  additionalStyles: PropTypes.object,
  invalid: PropTypes.bool,
}
