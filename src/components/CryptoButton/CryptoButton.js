import React from 'react'

import './CryptoButton.css'
import PropTypes from 'prop-types'

const CryptoButton = ({ text, isActive, handleOnClick }) => (
  <button
    type="button"
    onClick={handleOnClick}
    className={`crypto-button ${isActive ? 'active' : ''}`}
  >
    {text}
  </button>
)

export default CryptoButton

CryptoButton.propTypes = {
  text: PropTypes.string, // button text content
  isActive: PropTypes.bool, // button state
  handleOnClick: PropTypes.func,
}
