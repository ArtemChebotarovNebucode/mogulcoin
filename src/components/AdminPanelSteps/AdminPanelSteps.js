import React from 'react'

import './AdminPanelSteps.css'
import PropTypes from 'prop-types'

const AdminPanelSteps = ({ steps, activeStepIndex, additionalStyles }) => (
  <div className="step-mark" style={additionalStyles}>
    {steps.map((step, index) => (
      <div
        style={{
          ...(index !== steps.length - 1 ? { marginRight: 80 } : {}),
          ...(index !== activeStepIndex ? { opacity: 0.6 } : {}),
        }}
      >
        <p className="step-sign">step {index + 1}</p>
        <p className="white-text" style={{ margin: 0 }}>
          {step}
        </p>
      </div>
    ))}
  </div>
)

export default AdminPanelSteps

AdminPanelSteps.propTypes = {
  steps: PropTypes.array,
  activeStepIndex: PropTypes.number, // index of active step in steps array
  additionalStyles: PropTypes.object,
}
