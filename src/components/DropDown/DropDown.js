import React, { useState } from 'react'

import './DropDown.css'
import PropTypes from 'prop-types'

import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import pointerIcon from '../../assets/vector/pointer-icon.svg'
import pointerIconPink from '../../assets/vector/pointer-icon-pink.svg'
import CheckBoxInput from '../CheckBoxInput/CheckBoxInput'
import { TextInput } from '../index'

const DropDown = ({
  options,
  label,
  type,
  additionalStyle,
  startDate,
  setStartDate,
  endDate,
  setEndDate,
  onAmountFromChange,
  onAmountToChange,
  amountFrom,
  amountTo,
  title,
  minDate,
  maxDate,
  onChange,
}) => {
  const [dropDownFocus, setDropDownFocus] = useState(false)
  const [chosenValue, setChosenValue] = useState('')

  const onDateChange = (dates) => {
    const [start, end] = dates
    setStartDate(start)
    setEndDate(end)
  }

  if (type === 'multiple-choice') {
    return (
      <div className="drop-down" style={additionalStyle}>
        <div className="drop-down-input-wrapper">
          <input type="text" disabled placeholder={label} onChange={onChange} />
          <button
            type="button"
            onClick={() => setDropDownFocus(!dropDownFocus)}
          >
            <img src={pointerIcon} alt="Drop Down" />
          </button>
        </div>
        <div
          className="drop-down-panel"
          style={dropDownFocus ? { display: 'block' } : { display: 'none' }}
        >
          {options.map((option, index) => (
            <CheckBoxInput
              label={option.name}
              id={option.name}
              handleOnCheck={option.handleOnCheck}
              additionalStyles={
                index !== options.length - 1 ? { marginBottom: 20 } : {}
              }
              isChecked={option.checked}
            />
          ))}
        </div>
      </div>
    )
  }

  if (type === 'date-range') {
    return (
      <div className="drop-down" style={additionalStyle}>
        <div className="drop-down-input-wrapper">
          <input type="text" disabled placeholder={label} onChange={onChange} />
          <button
            type="button"
            onClick={() => setDropDownFocus(!dropDownFocus)}
          >
            <img src={pointerIcon} alt="Drop Down" />
          </button>
        </div>

        <div
          className="drop-down-panel"
          style={
            dropDownFocus
              ? {
                  maxHeight: 'initial',
                  padding: 20,
                  display: 'flex',
                  justifyContent: 'center',
                }
              : { display: 'none' }
          }
        >
          <DatePicker
            selected={startDate}
            onChange={onDateChange}
            startDate={startDate}
            endDate={endDate}
            selectsRange
            inline
          />
        </div>
      </div>
    )
  }

  if (type === 'number-range') {
    return (
      <div className="drop-down" style={additionalStyle}>
        <div className="drop-down-input-wrapper">
          <input type="text" disabled placeholder={label} onChange={onChange} />
          <button
            type="button"
            onClick={() => setDropDownFocus(!dropDownFocus)}
          >
            <img src={pointerIcon} alt="Drop Down" />
          </button>
        </div>

        <div
          className="drop-down-panel"
          style={dropDownFocus ? { display: 'flex' } : { display: 'none' }}
        >
          <TextInput
            styleId={4}
            additionalStyle={{ margin: 0 }}
            onChange={onAmountFromChange}
            value={amountFrom}
          />
          <p
            className="white-text"
            style={{
              margin: '0 10px 4px 10px',
              height: 'fit-content',
              alignSelf: 'center',
            }}
          >
            -
          </p>
          <TextInput
            styleId={4}
            additionalStyle={{ margin: 0 }}
            onChange={onAmountToChange}
            value={amountTo}
          />
        </div>
      </div>
    )
  }

  if (type === 'single-choice') {
    return (
      <div className="drop-down" style={additionalStyle}>
        <div className="drop-down-input-wrapper__pink">
          {title ? <p className="input-title">{title}</p> : null}
          <input type="text" disabled placeholder={label} value={chosenValue} />
          <button
            type="button"
            onClick={() => setDropDownFocus(!dropDownFocus)}
            style={{ marginRight: 18 }}
          >
            <img src={pointerIconPink} alt="Drop Down" />
          </button>
        </div>

        <div
          className="drop-down-panel"
          style={dropDownFocus ? { display: 'block' } : { display: 'none' }}
        >
          {options.map((option) => (
            <button
              className="white-text prefix-option"
              style={{
                margin: 0,
                whiteSpace: 'nowrap',
                lineHeight: '32px',
                width: '100%',
                textAlign: 'left',
              }}
              type="button"
              onClick={() => {
                setChosenValue(option)
                onChange(option)
              }}
            >
              {option}
            </button>
          ))}
        </div>
      </div>
    )
  }

  if (type === 'date-picker') {
    return (
      <div className="drop-down" style={additionalStyle}>
        <div className="drop-down-input-wrapper__pink">
          {title ? <p className="input-title">{title}</p> : null}
          <input
            type="text"
            disabled
            placeholder={label}
            value={
              startDate
                ? `${startDate.getFullYear()}/${
                    startDate.getMonth() < 10
                      ? `0${startDate.getMonth()}`
                      : startDate.get()
                  }/${
                    startDate.getDate() < 10
                      ? `0${startDate.getDate()}`
                      : startDate.getDate()
                  }`
                : null
            }
            onChange={onChange}
          />
          <button
            type="button"
            onClick={() => setDropDownFocus(!dropDownFocus)}
            style={{ marginRight: 18 }}
          >
            <img src={pointerIconPink} alt="Drop Down" />
          </button>
        </div>

        <div
          className="drop-down-panel"
          style={
            dropDownFocus
              ? {
                  maxHeight: 'initial',
                  padding: 20,
                  display: 'flex',
                  justifyContent: 'center',
                  width: 'fit-content',
                  right: 0,
                }
              : { display: 'none' }
          }
        >
          <DatePicker
            selected={startDate}
            onChange={(date) => setStartDate(date)}
            inline
            minDate={minDate}
            maxDate={maxDate}
          />
        </div>
      </div>
    )
  }

  return null
}

export default DropDown

DropDown.propTypes = {
  options: PropTypes.array,
  label: PropTypes.string,
  type: PropTypes.string,
  additionalStyle: PropTypes.object,
  startDate: PropTypes.object,
  setStartDate: PropTypes.func,
  setEndDate: PropTypes.func,
  endDate: PropTypes.object,
  onAmountFromChange: PropTypes.func,
  onAmountToChange: PropTypes.func,
  amountFrom: PropTypes.number,
  amountTo: PropTypes.number,
  title: PropTypes.string,
  minDate: PropTypes.object,
  maxDate: PropTypes.object,
  onChange: PropTypes.func,
}
