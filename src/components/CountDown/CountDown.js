// eslint-disable-next-line no-unused-vars
import React, { useEffect, useState } from 'react'

import './CountDown.css'
import PropTypes from 'prop-types'

const CountDown = ({ dateToCountDown }) => {
  let interval
  const [datesDifference, setDateDifference] = useState(
    dateToCountDown - new Date(),
  )

  useEffect(() => {
    interval = setInterval(() => {
      setDateDifference((prev) => prev - 1000)
    }, 1000)

    return () => {
      clearInterval(interval)
    }
  }, [])

  return (
    <div className="count-down">
      <p className="white-text">Ends in</p>
      <p className="time">{new Date(datesDifference).getDate()}</p>
      <p className="white-text">Days</p>
      <p className="time">{new Date(datesDifference).getHours()}</p>
      <p className="white-text">h</p>
      <p className="time">{new Date(datesDifference).getMinutes()}</p>
      <p className="white-text">m</p>
      <p className="time">{new Date(datesDifference).getSeconds()}</p>
      <p className="white-text">s</p>
    </div>
  )
}

CountDown.propTypes = {
  dateToCountDown: PropTypes.object, // date to which time will be counted down
}

export default CountDown
