import React from 'react'

import './AccessMethod.css'
import PropTypes from 'prop-types'

const AccessMethod = ({ text, isActive, handleOnClick, additionalStyles }) => (
  <button
    type="button"
    onClick={handleOnClick}
    className="access-method-button"
    style={{
      ...(isActive
        ? { backgroundColor: '#D10286' }
        : { backgroundColor: '#2A1A41' }),
      ...additionalStyles,
    }}
  >
    <p
      className="access-method-button-text"
      style={isActive ? { color: 'white' } : { color: '#705F87' }}
    >
      {text}
    </p>
  </button>
)

export default AccessMethod

AccessMethod.propTypes = {
  text: PropTypes.string, // button text content
  isActive: PropTypes.bool, // button state
  handleOnClick: PropTypes.func,
  additionalStyles: PropTypes.object,
}
