import React from 'react'

import './StatusBar.css'
import PropTypes from 'prop-types'

const StatusBar = ({ percent, from, to }) => (
  <div className="status-bar">
    <h4 className="subtitle-white" style={{ fontSize: 18, margin: 0 }}>
      {from}
    </h4>
    <div className="progress">
      <div id="remaining" />
      <div id="done" style={{ width: `${percent}%` }} />
    </div>
    <p className="subtitle-white" style={{ fontSize: 18, margin: 0 }}>
      {to}
    </p>
  </div>
)

export default StatusBar

StatusBar.propTypes = {
  percent: PropTypes.number, // percentage of filled bar,
  from: PropTypes.string, // left label
  to: PropTypes.string, // right label
}
