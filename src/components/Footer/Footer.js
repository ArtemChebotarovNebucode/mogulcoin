import React from 'react'

import facebookLogo from '../../assets/vector/facebook-logo.svg'
import twitterLogo from '../../assets/vector/twitter-logo.svg'
import telegramLogo from '../../assets/vector/telegram-logo.svg'
import instagramLogo from '../../assets/vector/instagram-logo.svg'
import linkedinLogo from '../../assets/vector/linkedin-logo.svg'
import discordLogo from '../../assets/vector/discord-logo.svg'
import logo from '../../assets/vector/logo.svg'
import footerBackground from '../../assets/raster/footer-background.png'

import './Footer.css'

const Footer = () => (
  <div className="footer">
    <div className="footer-content">
      <h3 className="footer-title" style={{ marginLeft: 18 }}>
        GOT{' '}
        <span style={{ color: '#D10286' }} className="blink">
          QUESTIONS?
        </span>
      </h3>
      <div className="contact-info">
        <a href="mailto:contact@mogulcoin.io" className="contact-link">
          <h4 className="footer-title email">contact@mogulcoin.io</h4>
        </a>
        <a
          href="https://www.facebook.com/joinmogulcoin/"
          className="contact-link"
          style={{ marginRight: 20 }}
        >
          <img src={facebookLogo} alt="Facebook" />
        </a>
        <a
          href="https://twitter.com/mogul_coin"
          className="contact-link"
          style={{ marginRight: 15 }}
        >
          <img src={twitterLogo} alt="Twitter" />
        </a>
        <a
          href="https://t.me/joinchat/KezM7YTByqRkMGU0"
          className="contact-link"
          style={{ marginRight: 18 }}
        >
          <img src={telegramLogo} alt="Telegram" />
        </a>
        <a
          href="https://www.instagram.com/mogul_coin/"
          className="contact-link"
          style={{ marginRight: 16 }}
        >
          <img src={instagramLogo} alt="Instagram" />
        </a>
        <a
          href="https://www.linkedin.com/company/mogul-coin/"
          className="contact-link"
          style={{ marginRight: 13 }}
        >
          <img src={linkedinLogo} alt="LinkedIn" />
        </a>
        <a
          href="https://discord.com/channels/829845747879903263/829845747879903266"
          className="contact-link"
        >
          <img src={discordLogo} alt="Discord" />
        </a>
      </div>
    </div>

    <hr className="footer-separator" />

    <div className="footer-content" style={{ padding: '80px 0' }}>
      <a href="/#" id="logo-footer">
        <img src={`${logo}`} alt="logo" />
      </a>
      {/* <div className="tabs"> */}
      {/*  <a className="tab" href="/#"> */}
      {/*    Information */}
      {/*  </a> */}
      {/*  <a className="tab" href="/#"> */}
      {/*    MGL Token */}
      {/*  </a> */}
      {/*  <a className="tab" href="/#"> */}
      {/*    Invest in MGL */}
      {/*  </a> */}
      {/*  <a className="tab" href="/#"> */}
      {/*    Tokenomics */}
      {/*  </a> */}
      {/*  <a className="tab" href="/#"> */}
      {/*    Timeline */}
      {/*  </a> */}
      {/*  <a className="tab" href="/#"> */}
      {/*    Affiliate */}
      {/*  </a> */}
      {/*  <a className="tab" href="/#"> */}
      {/*    Legal */}
      {/*  </a> */}
      {/* </div> */}
      <a href="/#" id="logo-footer-bottom">
        <img src={`${logo}`} alt="logo" />
      </a>
    </div>
    <img src={footerBackground} alt="Footer" height={50} />
  </div>
)

export default Footer
