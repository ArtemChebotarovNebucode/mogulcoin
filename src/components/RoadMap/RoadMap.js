import React, { useState } from 'react'
import Slider from 'react-slick'
import PropTypes from 'prop-types'

import leftArrow from '../../assets/vector/left-arrow.svg'
import rightArrow from '../../assets/vector/right-arrow.svg'
import roadMarkActive from '../../assets/vector/road-mark-active.svg'
import roadMarkDisabled from '../../assets/vector/road-mark-disabled.svg'
import tint from '../../assets/raster/roadmap-tint.png'

import './RoadMap.css'
import { useMain } from '../../contexts/MainProvider/MainProvider'

const SampleNextArrow = ({ className, onClick, isRightArrowDisplayed }) => (
  <button
    className={className}
    onClick={onClick}
    type="button"
    style={!isRightArrowDisplayed ? { display: 'none' } : {}}
  >
    <img src={rightArrow} alt="Next" width={28} height={13} />
  </button>
)

SampleNextArrow.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  isRightArrowDisplayed: PropTypes.bool,
}

const SamplePrevArrow = ({ className, onClick, isLeftArrowDisplayed }) => (
  <button
    className={className}
    onClick={onClick}
    type="button"
    style={!isLeftArrowDisplayed ? { display: 'none' } : {}}
  >
    <img src={leftArrow} alt="Prev" width={28} height={13} />
  </button>
)

SamplePrevArrow.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  isLeftArrowDisplayed: PropTypes.bool,
}

// eslint-disable-next-line react/prop-types
const RoadMap = ({ roadMapRef }) => {
  const { windowWidth } = useMain()
  const [isLeftArrowDisplayed, setIsLeftArrowDisplayed] = useState(false)
  const [isRightArrowDisplayed, setIsRightArrowDisplayed] = useState(true)

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: windowWidth > 972 ? 3 : 1,
    slidesToScroll: 1,
    nextArrow: (
      <SampleNextArrow isRightArrowDisplayed={isRightArrowDisplayed} />
    ),
    prevArrow: <SamplePrevArrow isLeftArrowDisplayed={isLeftArrowDisplayed} />,
    afterChange: (index) => {
      const slidesCount = windowWidth > 972 ? 3 : 1

      if (index === data.length - slidesCount) {
        setIsRightArrowDisplayed(false)
      } else {
        setIsRightArrowDisplayed(true)
      }

      if (index === 0) {
        setIsLeftArrowDisplayed(false)
      } else {
        setIsLeftArrowDisplayed(true)
      }
    },
  }

  const data = [
    {
      description:
        'Establishing the MOGUL Coin idea and core team. Researching and laying out the long-term strategy of the project.',
      title: 'Q4 2020',
      highlighted: false,
    },
    {
      description:
        'Creating the whitepaper and roadmap for MOGUL. Expanding the business development team.',
      title: 'Q1 2021',
      highlighted: false,
    },
    {
      description: 'The MOGUL ICO',
      title: 'MAY-JULY 2021',
      highlighted: true,
      extraLarge: true,
    },
    {
      description:
        'Beginning a series of podcasts with the streamers participating in the project. Participating in some of the largest conferences in both the global blockchain and streaming industries.',
      title: 'Q3 2021',
      highlighted: false,
    },
    {
      description:
        'Announcing the first corporate partnerships with brands. Efforts to get listed at several minor and major exchanges.',
      title: 'Q4 2021',
      highlighted: false,
    },
    {
      description:
        'Building the first MVP for our brand partners, including brands for the automatics placement of ads. Building the second part of the platform, including the NFT generator.',
      title: 'Q1 2022',
      highlighted: false,
    },
    {
      description:
        'Releasing the NFT marketplace. Hosting the first auctions on the NFT marketplace. Continued outreach to exchanges in an effort to increase the availability of MGUL.',
      title: 'Q2 2022',
      highlighted: false,
    },
    {
      description: 'Releasing the full version of the MOGUL platform.',
      title: 'Q3 2022',
      highlighted: true,
    },
    {
      description:
        'Beginning our full-scale international marketing campaign to promote the platform.',
      title: 'Q4 2022',
      highlighted: false,
    },
    {
      description:
        'Sponsoring some of the largest blockchain and streaming conferences globally.',
      title: 'Q1 2023',
      highlighted: false,
    },
    {
      description:
        'Releasing the 2.0 version of the MOGUL platform with brand new updates.',
      title: 'Q2 2023',
      highlighted: true,
    },
  ]

  return (
    <div className="roadmap" ref={roadMapRef}>
      <img src={tint} alt="Tint" id="roadmap-tint" />
      <h2 className="roadmap-title" style={{ textAlign: 'center' }}>
        ROADMAP
      </h2>
      <div className="roadmap-content">
        <div className="slider-container">
          <hr className="road" />
          <Slider {...settings}>
            {/* <div className="way active"> */}
            {/*  <h2 className="roadmap-title way-title">Q4 2020</h2> */}
            {/*  <div className="way-description "> */}
            {/*    <img */}
            {/*      src={roadMarkActive} */}
            {/*      alt="Road Mark" */}
            {/*      className="road-mark" */}
            {/*    /> */}
            {/*    <p> */}
            {/*      Establishing the MOGUL Coin idea and core team. Researching */}
            {/*      and laying out the long-term strategy of the project. */}
            {/*    </p> */}
            {/*  </div> */}
            {/* </div> */}
            {/* <div className="way"> */}
            {/*  <p className="roadmap-title way-title">Q1 2021</p> */}
            {/*  <div className="way-description "> */}
            {/*    <img */}
            {/*      src={roadMarkDisabled} */}
            {/*      alt="Road Mark" */}
            {/*      className="road-mark disabled" */}
            {/*    /> */}
            {/*    <p> */}
            {/*      Creating the whitepaper and roadmap for MOGUL Expanding the */}
            {/*      business development team. */}
            {/*    </p> */}
            {/*  </div> */}
            {/* </div> */}
            {/* <div className="way"> */}
            {/*  <p className="roadmap-title way-title">Q2 2021</p> */}
            {/*  <div className="way-description "> */}
            {/*    <img */}
            {/*      src={roadMarkDisabled} */}
            {/*      alt="Road Mark" */}
            {/*      className="road-mark disabled" */}
            {/*    /> */}
            {/*    <p> */}
            {/*      Creating the whitepaper and roadmap for MOGUL Expanding the */}
            {/*      business development team */}
            {/*    </p> */}
            {/*  </div> */}
            {/* </div> */}
            {/* <div className="way"> */}
            {/*  <p className="roadmap-title way-title">Q4 2021</p> */}
            {/*  <div className="way-description "> */}
            {/*    <img */}
            {/*      src={roadMarkDisabled} */}
            {/*      alt="Road Mark" */}
            {/*      className="road-mark disabled" */}
            {/*    /> */}
            {/*    <p> */}
            {/*      Creating the whitepaper and roadmap for MOGUL Expanding the */}
            {/*      business development team */}
            {/*    </p> */}
            {/*  </div> */}
            {/* </div> */}
            {/* <div className="way"> */}
            {/*  <p className="roadmap-title way-title">Q1 2022</p> */}
            {/*  <div className="way-description "> */}
            {/*    <img */}
            {/*      src={roadMarkDisabled} */}
            {/*      alt="Road Mark" */}
            {/*      className="road-mark disabled" */}
            {/*    /> */}
            {/*    <p> */}
            {/*      Creating the whitepaper and roadmap for MOGUL Expanding the */}
            {/*      business development team */}
            {/*    </p> */}
            {/*  </div> */}
            {/* </div> */}
            {/* <div className="way"> */}
            {/*  <p className="roadmap-title way-title">Q2 2022</p> */}
            {/*  <div className="way-description "> */}
            {/*    <img */}
            {/*      src={roadMarkDisabled} */}
            {/*      alt="Road Mark" */}
            {/*      className="road-mark disabled" */}
            {/*    /> */}
            {/*    <p> */}
            {/*      Creating the whitepaper and roadmap for MOGUL Expanding the */}
            {/*      business development team */}
            {/*    </p> */}
            {/*  </div> */}
            {/* </div> */}

            {data.map((way) => (
              <div className={`way ${way.highlighted ? 'active' : ''}`}>
                <p
                  className={`roadmap-title way-title ${
                    way.extraLarge ? 'extra-large' : ''
                  }`}
                >
                  {way.title}
                </p>
                <div className="way-description ">
                  <img
                    src={way.highlighted ? roadMarkActive : roadMarkDisabled}
                    alt="Road Mark"
                    className={`road-mark ${way.highlighted ? '' : 'disabled'}`}
                  />
                  <p>{way.description}</p>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </div>
  )
}

export default RoadMap
