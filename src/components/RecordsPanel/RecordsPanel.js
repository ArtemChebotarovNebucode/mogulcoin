import React from 'react'

import './RecordsPanel.css'

const data = [
  {
    name: 'Round 1',
    amount: '100 000 USD',
    tokenPrice: '0,0125 USD',
    active: false,
  },
  {
    name: 'Round 2',
    amount: '100 000 USD',
    tokenPrice: '0,0125 USD',
    active: true,
  },
  {
    name: 'Round 3',
    amount: '100 000 USD',
    tokenPrice: '0,0125 USD',
    active: false,
  },
  {
    name: 'Round 4',
    amount: '100 000 USD',
    tokenPrice: '0,0125 USD',
    active: false,
  },
  {
    name: 'Round 5',
    amount: '100 000 USD',
    tokenPrice: '0,0125 USD',
    active: false,
  },
]

const RecordsPanel = () => (
  <div className="records-panel">
    {data.map((round, index) => (
      <div
        className={`record ${round.active ? 'active' : ''}`}
        style={
          !round.active
            ? {
                borderRight: `${
                  index !== data.length - 1 ? '1px solid #33254C' : ''
                }`,
                borderLeft: `${index !== 0 ? '1px solid #33254C' : ''}`,
              }
            : {}
        }
      >
        <div className="title-section">
          <p className="record-title">{round.name}</p>
          <p className="record-title" style={{ fontSize: 14 }}>
            {round.amount}
          </p>
        </div>
        <div className="value-section">
          <p className="record-text title">MGUL Token Price</p>
          <p className="record-text" style={{ fontSize: 24 }}>
            {round.tokenPrice}
          </p>
        </div>
      </div>
    ))}
  </div>
)

export default RecordsPanel
