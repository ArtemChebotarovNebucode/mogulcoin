import React, { useContext, useState, useEffect } from 'react'
import PropTypes from 'prop-types'

const MainContext = React.createContext({})

export function useMain() {
  return useContext(MainContext)
}

const MainProvider = ({ children }) => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth)

  const handleOnWindowResize = () => {
    setWindowWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleOnWindowResize)
  }, [])

  const value = {
    windowWidth,
  }

  return <MainContext.Provider value={value}>{children}</MainContext.Provider>
}

export default MainProvider

MainProvider.propTypes = {
  children: PropTypes.array,
}
