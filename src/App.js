import React from 'react'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  // eslint-disable-next-line no-unused-vars
} from 'react-router-dom'

// eslint-disable-next-line no-unused-vars
import { Main, MainExtended } from './screens'
import { MainProvider } from './contexts'

import './App.css'
import Register from './screens/Register/Register'
import SignIn from './screens/SignIn/SignIn'
import {
  AllTransactions,
  Dashboard,
  DashboardExtended,
  PayWithProvider,
  Settings,
} from './screens/MyZone'
import KYC from './screens/KYC/KYC'
import CurrencyPurchase from './screens/CurrencyPurchase/CurrencyPurchase'
import CreateOrImportWallet from './screens/CreateOrImportWallet/CreateOrImportWallet'
import CreateWallet from './screens/CreateOrImportWallet/CreateWallet/CreateWallet'
import ImportWallet from './screens/CreateOrImportWallet/ImportWallet/ImportWallet'

const App = () => (
  <MainProvider>
    <Router>
      <Switch>
        <Route exact path="/">
          <Main />
        </Route>
        <Route path="/main-extended">
          <MainExtended />
        </Route>
        <Route exact path="/dashboard">
          <Dashboard />
        </Route>
        <Route path="/dashboard-extended">
          <DashboardExtended />
        </Route>
        <Route path="/dashboard/all-transactions">
          <AllTransactions />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/login">
          <SignIn />
        </Route>
        <Route path="/create-or-import-wallet">
          <CreateOrImportWallet />
        </Route>
        <Route path="/settings">
          <Settings />
        </Route>
        <Route path="/payment/pay-with-provider">
          <PayWithProvider />
        </Route>
        <Route path="/currency-purchase">
          <CurrencyPurchase />
        </Route>
        <Route path="/kyc">
          <KYC />
        </Route>
        <Route path="/create-wallet">
          <CreateWallet />
        </Route>
        <Route path="/import-wallet">
          <ImportWallet />
        </Route>
      </Switch>
    </Router>
  </MainProvider>
)

export default App
